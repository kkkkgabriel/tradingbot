from __init__ import *
import os
import datetime
import pandas as pd

'''
The functions in this folder collects data from the respective api and stores them in the DATA_FOLDER in the
following structure:

DATA
- {PLATFORM}
	- {GRANULARITIES[0]}
		- {dates[0]}
			- {pairs[0]}
			- {pairs[1]}
			- {pairs[2]}
		- {dates[1]}
		- {dates[2]}
		...
	- {GRANULARITIES[1]}
	- {GRANULARITIES[2]}
	...

Dates are in a rfc3339 format YYYY-MM-DD. 
'''

def main():
	'''
	main function to run if this script is run-ed.
	'''
	get_all_data_for_day('20211027000000')
	# get_all_data_for_month('20210501000000')
	# data_all_data_for_period('20211027000000', '20211026000000')
  

def create_data_folders(verbose=False):
	'''
	Create folders for raw data. The platform is set in config.py. Depending on the platform set,
	The GRANULARITIES will be retrieved from the respective platform folder, defined in _env.py

	Eg:
	data/{PLATFORM}/
	- {GRANULARITIES[0]}
	- {GRANULARITIES[1]}
	- {GRANULARITIES[2]}
	...

	Parameters
	------------
	verbose : bool
		if true, prints the number of folders created

	Returns
	------------
	folders_create : int
		The number of folders created.
	'''
	folders_created = 0

	# create platform folder if platform folder not yet created
	if PLATFORM not in os.listdir(DATA_FOLDER):
		os.mkdir(DATA_FOLDER + PLATFORM)
		folders_created += 1

	# loop through all granularities for platform
	# create granularity folder if not created
	granularity_files = os.listdir(DATA_FOLDER + PLATFORM)
	for granularity in GRANULARITIES.keys():
		if granularity not in granularity_files:
			os.mkdir(DATA_FOLDER + PLATFORM + '/' + granularity)
			folders_created += 1

	# print message if verbose
	if verbose:
		print('Folders created: {}'.format(folders_created))

	return folders_created

def create_day_data_folder(date):
	'''
	Creates all the day folders in all the granularity folder.

	Parameters
	------------
	date : str
		The date that are to be the name of the folders

	returns
	------------
	folders_created : int
		The number of folders that have been created. If its the first run for a particular date,
		the number of folders should be the same as the number of granularities defined in _env.py.
	'''
	try:
		folders_created = 0

		# loop through all granularity folders, create date folder if not yet created
		for granularity in GRANULARITIES.keys():
			if date not in os.listdir(DATA_FOLDER + PLATFORM + '/' + granularity):
				os.mkdir(DATA_FOLDER + PLATFORM + '/' + granularity + '/' + date)
				folders_created += 1
		return folders_created
	except Exception as e:
		raise e

def get_particular_data(instrument, granularity_key, start, end):
	'''
	Calls the candles api from the respective platform api file. The candles retrieved will be between the start and end parameters.
	Saves the data in a csv file in the folder named {instrument}, in the granularity folder. 

	Parameters
	------------
	instrument : str
		One of the instruments defined in the respective platform _env file
	granularity_key : str
		The key of the granularity defined in the respective platform _env file
	start : str
		The start time for which the retrieved data should start, in the rfc3339 format YYYY-MM-DD
	end : str
		The end time for which the retrieved data should end, in the rfc3339 format YYYY-MM-DD
	'''
	# make the api call
	r = call_get_candles_api(
		instrument,
		granularity=GRANULARITIES[granularity_key],
		start=start,
		end=end
	)
	print('\t{} at {} b/w {} - {}: '.format(instrument, granularity_key, start, end), r)
	try:
		# parse data to wanted format
		data = r.json()['candles']
		data = [{
			'complete': datum['complete'],
			'volume': datum['volume'],
			'time': ctime.from_rfc3339(datum['time']),
			'open': datum['mid']['o'],
			'high': datum['mid']['h'],
			'low': datum['mid']['l'],
			'close': datum['mid']['c']
		} for datum in data]

		# make df
		df = pd.DataFrame.from_dict(data)

		# sort according to datetime
		df.sort_values(by=['time'], inplace=True)

		df.to_csv(DATA_FOLDER + PLATFORM + '/' + granularity_key + '/' + start + '/' + instrument +'.csv')

	except Exception as e:
		print(e)

def get_all_data_for_granularity(granularity_key, start, end):
	'''
	Calls get_particular_data() to retrieve all candle csvs for all instrument on a particular granularity depending on start and end.
	Saves all retrieved data in folders.

	Parameters
	------------
	instrument : str
		One of the instruments defined in the respective platform _env file
	granularity_key : str
		The key of the granularity defined in the respective platform _env file
	start : str
		The start time for which the retrieved data should start, in the rfc3339 format YYYY-MM-DD
	end : str
		The end time for which the retrieved data should end, in the rfc3339 format YYYY-MM-DD
	'''
	print('looping through all instruments for {}, b/w {} - {}'.format(granularity_key, start, end))

	# loop through all instruments and retrieve data
	for instrument in INSTRUMENTS:
		get_particular_data(instrument, granularity_key, start, end)

def get_all_data_for_day(date):
	'''
	Retrieves all candles csvs for all instruments and all granularities for a day.

	Parameters
	------------
	date : str
		The date of data to be retrieved in the custom format
	'''

	# get start and end date in rfc3339 format
	start = ctime(date).to_rfc3339_date()
	end = ctime(date).add(1, 'D').to_rfc3339_date()
	print('\n-------- retrieving data for {} - {} ----------'.format(start, end))

	# create all folders for date
	folders_created = create_day_data_folder(start)
	if folders_created > 0:
		print('created {} folders for above date'.format(folders_created))

	# loop through all granularities to retrieve all data for that day
	for granularity_key in list(GRANULARITIES.keys()):
		get_all_data_for_granularity(granularity_key, start, end)

def get_all_data_for_month(start_date):
	'''
	Retrieves all csvs for a month (all days, all instruments, all granularities)

	Parameters
	------------
	start_date : str 
		date of starting month
	'''

	# get the time object of the start and end (a month after start date)
	start = ctime(start_date)
	end = ctime(start_date).add(1, 'M')

	# repeatedly increment start date till it reaches the end date
	# and retrieve the csv for that particular date
	while start < end:
		get_all_data_for_day(start.to_custom_datetime())
		start.add(1, 'D')

def data_all_data_for_period(start_date, end_date):
	'''
	Retrieves all csvs for a period between an indicated start and end (exclusive) (all instruments, all granularities)

	Parameters
	------------
	start_date : str 
		start date of chosen period

	end_date : str
		end date of chosen period (exclusive)
	'''

	# get the time object of the start and end (a month after start date)
	start = ctime(start_date)
	end = ctime(end_date)

	# repeatedly increment start date till it reaches the end date
	# and retrieve the csv for that particular date
	while start < end:
		get_all_data_for_day(start.to_custom_datetime())
		start.add(1, 'D')

if __name__ == '__main__':
	main()














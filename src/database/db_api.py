import requests

from __init__ import *
from database.db_connection import *
from database.objects import *

class db:

    #-------- bits ----------#
    '''
    Control bits
    - open: if true, positions will be opened
    - live: if true, the real positions will be opened (trading or demo) will be used
    '''
    @classmethod
    def read_open_bit(db):
        url = ROOT_URL + 'bits/open.json'
        response = authed_session.get(url)

        return response.json()

    @classmethod
    def read_live_bit(db):
        url = ROOT_URL + 'bits/live.json'
        response = authed_session.get(url)

        return response.json()

    #-------- prediction crud -----------#
    @classmethod
    def create_new_prediction(db, prediction):
        '''
        create new prediction

        Parameters 
        ------------
        prediction : objects.Prediction

        Returns
        ------------
        : str
            'name'/prediction_id
        success : boolean
            true if prediction is successfully created (to be implemented)
        '''
        url = ROOT_URL + 'predictions.json'
        response = authed_session.post(url, json=prediction.to_json())
            
        # todo: error handling
        success = True
        
        return response.json()['name'], success

    @classmethod
    def read_prediction(db, pkey):
        '''
        read prediction

        Parameters
        ------------
        pkey : str
            primary key of prediction to read

        Returns
        ------------
        prediction : objects.Prediction
            prediction object
        '''
        url = ROOT_URL + 'predictions/{}.json'.format(pkey)
        response = authed_session.get(url)
        prediction = Prediction.from_firebase((pkey, response.json()))        
        # todo: error handling
        
        return prediction

    @classmethod
    def read_all_predictions(db):
        '''
        read all predictions

        Returns
        ------------
        predictions : list
            list of predictions (objects.Prediction)
        '''
        url = ROOT_URL + 'predictions.json'
        response = authed_session.get(url)
        predictions = list(response.json().items())
        predictions = [Prediction.from_firebase(prediction) for prediction in predictions]

        # todo: filter and sort        
        # todo: error handling

        return predictions

    @classmethod
    def update_prediction(db, prediction):
        '''
        update prediction

        Parameters 
        -----------
        prediction : objects.Prediction
            updated prediction object

        Returns
        -----------
        success : boolean
            true if prediction has been successfully updated (to be implemented)
        '''
        url = ROOT_URL + 'predictions/{}.json'.format(prediction.pkey)
        response = authed_session.patch(url, json=prediction.to_json())
                
        # todo: error handling
        success = True
        
        return success

    #--------- position crud ----------------#
    @classmethod
    def create_new_position(db, position):
        '''
        create new position

        Parameters
        -----------
        position : objects.Position
            position object to stored in the db

        Returns
        -----------
        : str
            primary key of the created position
        success : bool
            true if position is successfully created
        '''
        url = ROOT_URL + 'positions.json'
        response = authed_session.post(url, json=position.to_json())
                
        # todo: error handling
        success = True
        
        return response.json()['name'], success

    @classmethod
    def read_position(db, pkey):
        '''
        read position with primary key

        Parameters
        -----------
        pkey : str
            primary key of the position to read

        Returns
        -----------
        position : objects.Position
            position object that is retrieved
        '''
        url = ROOT_URL + 'positions/{}.json'.format(pkey)
        response = authed_session.get(url)
        position = Position.from_firebase((pkey, response.json()))        
        # todo: error handling
            
        return position

    @classmethod
    def read_all_positions(db):
        '''
        read all positions

        Returns
        -----------
        positions : list
            list of positions (objects.Position)
        '''
        url = ROOT_URL + 'positions.json'
        response = authed_session.get(url)
        json = response.json()
        if json == None:
            print('No positions found')
            return []
            
        positions = list(response.json().items())
        positions = [Position.from_firebase(position) for position in positions]

        # todo: filter and sort        
        # todo: error handling

        return positions

    @classmethod
    def update_position(db, position):
        '''
        update position

        Parameters
        -----------
        position : object.Position
            updated position object

        Returns
        -----------
        success : bool
            true if position has been successfully updated (to be implemented)
        '''
        url = ROOT_URL + 'positions/{}.json'.format(position.pkey)
        response = authed_session.patch(url, json=position.to_json())
                
        # todo: error handling
        success = True
        
        return success

    #-------- merchant crud -----------------#
    @classmethod
    def create_new_merchant(db, merchant):
        '''
        create new merchant

        Parameters
        -----------
        merchant : Merchant
            merchant object to be stored 

        Returns
        -----------
        : str
            attributes of the merchant
        success : bool
            true if merchant has been created and stored
        '''
        url = ROOT_URL + 'merchants/{}.json'.format(str(merchant))
        json = {
            'active': False,
            'true_positives': 0,
            'predicted_positives': 0,
            'datetime_of_last_prediction': 'na',
            'position_id': 'na'
        }
        response = authed_session.patch(url, json=json)

        # todo: error handling
        success = True

        return response.json(), success

    @classmethod
    def update_merchant(db, merchant, json):
        '''
        update merchant

        Parameters
        -----------
        merchant : Merchant
            updated merchant object
        json : dictionary
            attributes of the merchant to patch

        Returns
        -----------
        : str
            attributes of the merchant
        success : bool
            true if merchant has been updated
        '''
        url = ROOT_URL + 'merchants/{}.json'.format(str(merchant))
        response = authed_session.patch(url, json=json)

        # todo: error handling
        success = True

        return response.json(), success

    @classmethod
    def read_merchant(db, merchant):
        '''
        read merchant

        Parameters
        -----------
        merchant : Merchant
            Merchant object to read from db

        Returns
        -----------
        : dictionary
            merchant attributes
        success : bool
            true if merchant attributes has been retrieved
        '''
        url = ROOT_URL + 'merchants/{}.json'.format(str(merchant))
        response = authed_session.get(url)

        # todo: error handling
        success = True

        return response.json(), success

    #----------- accounts snapshot crud ------------#
    @classmethod
    def create_new_account_snapshot(db, account):
        '''
        create new account snapshot

        Parameters
        -----------
        account : Accountant object

        Returns
        -----------
        : dictionary
            account snapshot details
        success : bool
            true if account snapshot has been stored
        '''
        url = ROOT_URL + 'account_snapshots/{}.json'.format(str(account))
        response = authed_session.patch(url, json=account.to_json())

        # todo: error handling
        success = True

        return response.json(), success

    @classmethod
    def read_all_account_snapshots(db):
        '''
        read all account snapshots

        : dictionary
            dictionary of all account snapshots
        success : bool
            true if all snapshots has been retrieved successfully
        '''
        url = ROOT_URL + 'account_snapshots.json?orderBy="$key"'
        response = authed_session.get(url)

        # todo: error handling
        success = True

        return response.json(), success

    @classmethod
    def read_latest_account_snapshot(db):
        '''
        read latest account snapshot

        : dictionary
            attributes of latest account snapshots
        success : bool
            true if latest account snapshots has been retrieved successfully
        '''
        url = ROOT_URL + 'account_snapshots.json?orderBy="$key"&limitToLast=1'
        response = authed_session.get(url)

        # todo: error handling
        success = True

        return response.json(), success






















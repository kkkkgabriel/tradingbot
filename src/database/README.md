# Overview
My data is saved on realtime database on firebase. The database contains a collection of the following objects. See Schema section for the breakdown of each object.
- Objects
	- Account snapshots
	- Positions
	- Predictions
	- Merchants

# Schema
## Account snapshots
- balance
- margin_available
- margin_used
- n_open_positions
- profit_loss
- time_of_update

## Positions
- amount
- close_price
- closure_reason
- confidence
- date_close
- datetime_close
- datetime_open
- granularity_key
- instrument
- long_short
- open
- open_price
- pkey
- platform
- prediction_id
- profit_loss
- stop_loss
- take_profit

## Predictions
- confidence
- datetime_of_last_candle
- datetime_of_prediction
- datetime_of_termination
- granularity_key
- instrument
- model_class
- model_location
- output
- pkey
- platform
- predicted_price
- realised


## Merchants
- active
- datetime_of_last_prediction
- position_id
- predicted_positives
- true_positives
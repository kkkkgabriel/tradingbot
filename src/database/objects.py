from __init__ import *
class DBObject:
    def to_json(self):
        return self.__dict__
    
class Position(DBObject):
    def __init__(
        self, 
        granularity_key,
        instrument,
        amount,
        long_short,
        open_price,
        datetime_open,
        prediction_id,
        platform,
        stop_loss,
        take_profit,
        closure_reason='na',
        open=True,
        confidence=0,
        close_price='na',
        datetime_close='na',
        date_close='na',
        profit_loss='na',
        pkey='na'
    ):
        self.granularity_key = granularity_key
        self.instrument = instrument
        self.amount = amount
        self.long_short = long_short
        self.open = open
        self.open_price = open_price
        self.close_price = close_price
        self.datetime_open = datetime_open
        self.datetime_close = datetime_close
        self.date_close = date_close
        self.stop_loss = stop_loss
        self.take_profit = take_profit
        self.confidence = confidence
        self.closure_reason = closure_reason
        self.profit_loss = profit_loss
        self.prediction_id = prediction_id
        self.platform = platform
        self.pkey = pkey

    @classmethod
    def from_firebase(Position, tup):
    	pkey = tup[0]
    	values = tup[1]
    	return Position(
	        values['granularity_key'],
	        values['instrument'],
	        values['amount'],
	        values['long_short'],
	        values['open_price'],
	        values['datetime_open'],
	        values['prediction_id'],
	        values['platform'],
	        values['stop_loss'],
	        values['take_profit'],
	        closure_reason=values['closure_reason'],
	        confidence=values['confidence'],
	        date_close=values['date_close'],
	        open=values['open'],
	        close_price=values['close_price'],
	        datetime_close=values['datetime_close'],
	        profit_loss=values['profit_loss'],
	        pkey=pkey
    	)

class Prediction(DBObject):
	def __init__(
		self,
		granularity_key,
		instrument,
		model_location,
		model_class,
		datetime_of_last_candle,
		datetime_of_prediction,
		datetime_of_termination,
		predicted_price,
		platform,
		output,
		confidence,
		realised=False,
		pkey='na',
	):
		self.granularity_key = granularity_key
		self.instrument = instrument
		self.model_location = model_location
		self.model_class = model_class
		self.realised = realised
		self.datetime_of_last_candle = datetime_of_last_candle
		self.datetime_of_prediction = datetime_of_prediction
		self.datetime_of_termination = datetime_of_termination
		self.predicted_price = predicted_price
		self.platform = platform
		self.output = output
		self.confidence = confidence
		self.pkey = pkey

	@classmethod
	def from_firebase(Prediction, tup):
		pkey = tup[0]
		values = tup[1]
		return Prediction(
			values['granularity_key'],
			values['instrument'],
			values['model_location'],
			values['model_class'],
			values['datetime_of_last_candle'],
			values['datetime_of_prediction'],
			values['datetime_of_termination'],
			values['predicted_price'],
			values['platform'],
			values['output'],
			values['confidence'],
			realised=values['realised'],
			pkey=pkey
		)


































from __init__ import *
import deployment.settings as s

from accountant import Accountant
from merchant import Merchant

from datetime import datetime
'''
The company class/object simulates the running of a company.

The company has a few roles:
- accountant (x1): manages the companies accounts
- predictor (x1): makes prediction 
- merchants (xN, where N = depending on number of instruments to be traded): handles the opening and closing of positions

'''
class Company:
	def __init__(self):
		'''
		Initialises a Company object with all the roles
		'''
		self.accountant = None
		self.predictor = None
		self.merchants = []
		self.start()

	def start(self):
		'''
		sets the roles according to settings (settings.py)
		'''
		self.accountant = Accountant()
		self.__init_predictor()
		self.__init_merchants()

	def routine(self):
		'''
		routine for the entire company
		- merchants to check for positions to close and open
		- accountant to check if time for snapshots
		'''
		for merchant in self.merchants:
			merchant.routine()
		
		self.accountant.routine()

	def quality_check(self):
		'''
		quality check for company, merchants to check for stop loss and take profit
		'''
		if self.accountant.has_open_positions():
			for merchant in self.merchants:
				merchant.quality_check()

	def __init_predictor(self):
		'''
		inits the predictor
		'''
		if s.predictor_class == 'Prophet':
			self.predictor = eval(s.predictor_class).load_model(eval(s.model_class), s.model_path, s.n_inputs, threshold=s.threshold)
		elif s.predictor_class == 'Yin_yang_prophets':
			self.predictor = eval(s.predictor_class).load_model(
			    eval(s.yin_model_class),
			    eval(s.yang_model_class), 
			    s.yin_model_path,
			    s.yang_model_path,
			    s.n_inputs,
			    yin_threshold=s.yin_threshold,
			    yang_threshold=s.yang_threshold,
	            yin_ceiling_threshold=s.yin_ceiling_threshold,
	            yang_ceiling_threshold=s.yang_ceiling_threshold
			)
		else:
			raise Exception("Predictor init error!")


	def __init_merchants(self):
		'''
		inits the merchants
		'''
		for instrument in s.instruments:
			merchant = Merchant(s.n_candles, instrument, s.granularity_key)
			merchant.set_predictor(self.predictor)
			merchant.set_accountant(self.accountant)
			self.merchants.append(merchant)

	def get_merchants_info(self):
		'''
		get all merchants information
		'''
		for merchant in self.merchants:
			print('\n{}'.format(str(merchant)))

	# todo: close()
	# sets all the merchants' active bool to false
	# def close():
	# ...

	def get_info(self):
		return 'company'


def main():
	startTime = datetime.now()
	inv_co = Company()
	inv_co.routine()
	print('\tRuntime: {}'.format(datetime.now() - startTime))

if __name__ == '__main__':
	main()
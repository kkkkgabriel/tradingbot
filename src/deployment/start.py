import sys
from company import Company

from datetime import datetime

def main():
	'''
	if argument is 0, run routine, otherwise, run quality check
	'''
	if int(sys.argv[1]) == 0:
		routine()
	else:
		quality_check()

def routine():
	'''
	runs the company's routine
	'''
	startTime = datetime.now()
	inv_co = Company()
	inv_co.routine()
	print('\tRuntime: {}'.format(datetime.now() - startTime))

def quality_check():
	'''
	runs the company's quality check
	'''
	startTime = datetime.now()
	inv_co = Company()
	inv_co.quality_check()
	print('\tRuntime: {}'.format(datetime.now() - startTime))

if __name__ == '__main__':
	main()
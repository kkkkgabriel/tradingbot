from __init__ import *

#------ instrument details --------#
granularity_key = '15_minute'
instruments = [
    # eur
    'EUR_USD', 'EUR_GBP', 'EUR_AUD', 'EUR_NZD', 'EUR_CAD', 'EUR_CHF', 'EUR_SGD',

    # usd 
    'GBP_USD', 'AUD_USD', 'NZD_USD', 'USD_CAD', 'USD_CHF', 'USD_SGD',

    # gbp
    'GBP_AUD', 'GBP_NZD', 'GBP_CAD', 'GBP_CHF', 'GBP_SGD',

    # aud
    'AUD_NZD', 'AUD_CAD', 'AUD_CHF', 'AUD_SGD',

    # nzd
    'NZD_CAD', 'NZD_CHF', 'NZD_SGD',

    # cad
    'CAD_CHF', 'CAD_SGD',

    # chf
    'SGD_CHF'

    # left out JPY and HKD pairs
]

#------ merchant ------#
n_candles = 5
trade_units = 1000
target_pips = 10
order_wait_time = (10, 'm')
stop_loss_pips = 12 # 'na'
take_profit_pips = 18 # 'na'
allowable_open_pips = 5

#------ predictor ------#
predictor_class = 'Yin_yang_prophets' # Prophet || Yin_yang_prophets

#!!! prophet settings
# model_path = MODEL_FOLDER + '20211021104847_positive_non_bin_epoch_100'
# model_class = 'positive_linear_v1'
# n_inputs = 19
# threshold = 0.6

#!!! yin yang prophets settings
yang_threshold = 0.6
yang_model_path = MODEL_FOLDER + '20211025235418_positive_non_bin_v2_epoch_100'
yang_model_class = 'non_bin_v2'
yin_ceiling_threshold = 0.15

yin_threshold = 0.54
yin_model_path = MODEL_FOLDER + '20211026082249_negative_non_bin_v2_epoch_100'
yin_model_class = 'non_bin_v2'
yang_ceiling_threshold = 0.24

n_inputs = 19
model_path = yin_model_path + ' & ' + yang_model_path
model_class = yin_model_class + ' & ' + yang_model_class

#------ accountant ------#
snapshot_interval = (1, 'h')
max_open_positions = 10

#------ others ------#
sgt_temporal_offset = (8, 'h')
termination_offset = (90, 'm')
# termination_offset = (10, 'm')
from __init__ import *
import deployment.settings as s
'''
This accountant class/object manages the balances and margins of the company. 
It controls if the company if still in position to open more positions in the market.
He also periodically takes snapshots of the company's accounts, for reporting purposes.
'''

class Accountant:
	def __init__(self):
		'''
		Initialises a Accountant object
		'''
		self.balance = None
		self.margin_available = None
		self.margin_used = None
		self.time_of_update = None
		self.profit_loss = None
		self.n_open_positions = 0
		self.update()


	def routine(self):
		'''
		The routine of the accountant
		'''
		# check for the last update time
		last_update_time = self.get_last_update_time()

		# if last update time exists and is of sufficient interval from current time, take snapshot
		if last_update_time:
			now = ctime.from_now()
			if last_update_time.add(*s.snapshot_interval) < now:
				self.snapshot()
		else:
			self.snapshot()

	def update(self):
		'''
		Updates the database with the actual status of the account
		'''
		# call the get accounts api
		try:
			r = call_get_account_details_api().json()

			# set the attributes
			self.balance, self.margin_available, self.margin_used, self.profit_loss = parse_account_details(r)
			self.n_open_positions = parse_account_details_for_number_of_open_positions(r)
			self.time_of_update = str(ctime.from_now())
			return self
		except Exception as e:
			self.log('Error in parsing response from call_get_account_details_api: {}'.format(e))
			return self

	def update_n_open_positions(self, delta):
		'''
		Update number of open positions 

		Parameters
		-----------
		delta : int
			change in number of open positions, positive or negative
		'''
		self.n_open_positions += delta
		self.log("Current # of open positions: {}/{}".format(self.n_open_positions, s.max_open_positions))

	def get_permission_to_open_position(self):
		'''
		public function of merchants to call to get permission to open positions
		'''
		# check if number of open positions has exceeded max open positions
		granted = self.n_open_positions < s.max_open_positions

		# log
		if not granted:
			self.log('Max open position of {} hit. Request to open position denied.'.format(s.max_open_positions))
			
		return granted

	def has_open_positions(self):
		'''
		check if accountant has open positions
		'''
		return self.n_open_positions > 0
		
	def get_last_update_time(self):
		'''
		get the last update time
		'''
		# expected errors:
		# - first snapshot, no previous snapshot exist
		try:
			return ctime(list(db.read_latest_account_snapshot()[0].keys())[0])
		except Exception as e:
			return None

	def snapshot(self):
		'''
		takes a snapshot of the company's accounts, updates the database
		'''
		self.log('Taking snapshot')
		self.update()
		db.create_new_account_snapshot(self)

	def to_json(self):
		'''
		prints the accountant's attribute
		'''
		return self.__dict__

	def log(self, message):
		l.log(self.get_info(), message)

	def get_info(self):
		return 'accountant'

	def __str__(self):
		return self.time_of_update

# Overview
The deployed system is implemented as a python script that is run-ed in a loop at time intervals, specified in [start.sh](start.sh) The settings such as stop loss, take profit, model class, model weights and more can be set in [settings.py](settings.py).

# Architecture
The python scripts work together to simulate an investment company, at least a simple version of it. The employees as a company performs 1 out of 2 activities on every run: routine, quality check. The interaction of the employees in the company is illustrated below.     
## Routine
![routine image](../../assets/deployment_routine.png)
## Quality check
![qc image](../../assets/deployment_qc.png)
&nbsp;    
### Merchants
In the company, there are multiple merchants. Each merchant manages the trade of a single instrument (currency pair). The list of instruments is set in [settings.py](settings.py). The number of trade units, stop loss pips, take profit pips and more configurations for all of these merchants are set in [settings.py](settings.py).    

### Prophets
The prophet(s) is implemented as a predictor object (see [predict.py](../network_operations/predict.py)). The predictor's threshold values are settable as well. At the moment of writing, there are 2 classes of predictor:    
&nbsp;    
1. *Prophet*    
The prophet makes a single prediction with a single model. Current prophet implementations does a binary classification for a long (or short) position. A positive prediction would mean that a position should be opened, while a negative prediction would mean that it should not.   
&nbsp;    
2. *Yin_yang_prophets*    
A yin_yang_prophets object implements 2 prophets to work together. The yang prophet predicts if a long position should be opened, while the yin prophet predicts if a short position should be opened. A long position should only be opened if the yang prophets makes a positive prediction while the yin prophets makes a negative. Additionally, threshold values has to be met for all these positive and negative prediction for it to be actually be classified as a positive and negative prediction correspondingly.

### Accountant
The accountant manages the accounts of the company, making sure that the balances and available margins are sufficient for opening new positions. There is a maximum number of positions that can be opened that is settable. The accountant also occasionally takes snapshot of the accounts (from the platform's API). These snapshots are used for review purposes.

# Quick start
Run ```start.sh```
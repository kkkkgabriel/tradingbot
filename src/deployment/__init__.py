import sys

sys.path.append('..')

from config import *
from utils.ctime import ctime
from utils.globals import *
from utils.log import Logger as l

from network_operations.compile import *
from network_operations.interpret import *
from network_operations.mine import *
from network_operations.netMixin import *
from network_operations.retrieval import *
from network_operations.networks import *
from network_operations.split import *
from network_operations.predict import *

from database.db_api import *
from database.objects import *

# TODO: add conditions for import depending on platform
if PLATFORM == 'oanda':
	from oanda.api import *
	from oanda._env import *
#! /bin/bash
sleep_duration=30
running=true
counter=0
counter_limit=9


while [ running ]; do
    now=$(date)
    echo "Time of run: " $now

    python start.py $counter

    # increment/reset counter
    counter=$((counter+1))
    counter=$(($counter % $counter_limit))

    sleep $sleep_duration
done
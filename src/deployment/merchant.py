from __init__ import *
import deployment.settings as s

from torch import tensor

'''
The Merchant class/object handles the trading one particular instrument.
The merchant periodically checks the price history of the instrument and consults the predictor if a position should be opened.
If so, the merchant opens the position provided that he does not have any open positions at the moment.
'''

class Merchant:
	def __init__(
			self,
			n_candles,
			instrument,
			granularity_key,
			correct_time_to_sgt=True
		):
		# key attributes
		self.n_candles = n_candles
		self.instrument = instrument
		self.granularity_key = granularity_key
		self.position = None # this stores the actual position object

		# temporal offset for data timestamp
		self.temporal_offset = (0, 'h')
		if correct_time_to_sgt:
			self.temporal_offset = s.sgt_temporal_offset

		# colleagues
		self.predictor = None
		self.accountant = None

		# data
		self.latest_raw_data = None
		self.latest_processed_data = None
		self.latest_prediction = None # this stores the prediction_id
		self.latest_timestamp = None

	def routine(self):
		'''
		The routine of the merchant
		'''
		# update itself with the data on the database
		self.start()

		# check for closes
		if self.accountant.has_open_positions():
			close = False
			closure_reason = 'na'
			if self.check_for_potential_closes():
				self.log('Found potential close.')
				close = True
				closure_reason = 'Termination'
			else:
				close, closure_reason = self.check_early_closure()
				if close:
					self.log('Early closure detected: {}'.format(closure_reason))

			if close:
				if self.close_position(closure_reason):
					self.accountant.update_n_open_positions(-1)
					self.log('Successfully closed position.')
				else:
					self.log('!!! Something went wrong in closing position.')

		# check for opens
		if db.read_open_bit():
			# only check for potential opens when open bit is true, to reduce firebase usage,
			# and no need for that many predictions record anyway
			if self.check_for_potential_opens():
				self.log('Found potential open.')

				if self.accountant.get_permission_to_open_position():
					if self.open_position():
						self.accountant.update_n_open_positions(1)
						self.log('Successfully opened position.')
					else:
						self.log('Unable to open position.')

	def quality_check(self):
		'''
		check for stop loss or take profit positions
		'''
		# update it self with the data on the database
		self.start()

		# check for closes
		close, closure_reason = self.check_early_closure()
		if close:
			self.log('Early closure detected: {}'.format(closure_reason))
			if self.close_position(closure_reason):
				self.accountant.update_n_open_positions(-1)
				self.log('Successfully closed position.')

	def start(self):
		'''
		start the merchant object with the data stored on the db
		'''

		# init updated merchant, if merchant doesn't exist, create new one
		json, success = db.read_merchant(self)
		if json == None:
			json, success = db.create_new_merchant(self)

		# todo: update active bool to true

		# retrieve open position if available
		# Note: hard coded for oanda
		if success and json['position_id'] != 'na':
			self.position = db.read_position(json['position_id'])

		return self

	def check_for_potential_closes(self):
		'''
		check if any open positions can be closed
		'''
		# check if merchant has any open position
		if self.__holds_open_position():

			# if merchant has open position, get the corresponding prediction and check the prediction's termination datetime
			prediction = db.read_prediction(self.position.prediction_id)
			datetime_of_termination = prediction.datetime_of_termination
			return ctime(datetime_of_termination) < ctime.from_now()

		return False

	def check_early_closure(self):
		'''
		check for early closure in open position due to stop loss or take profit
		'''

		# make return values
		take_profit_return = (True, 'Take profit')
		stop_loss_return = (True, 'Stop loss')
		no_close_return = (False, None)

		# ensure that merchant holds open position
		if self.__holds_open_position():
			# check if position is long or short
			long_short = self.position.long_short

			# get stop loss and take profit values
			stop_loss = self.position.stop_loss
			take_profit = self.position.take_profit

			# set buy/sell: if long position, we are looking to sell; if short position, we are looking to buy
			buy_sell = 'sell' if long_short == 'long' else 'buy'

			# get latest price of position
			latest_price = call_get_price_api(self.instrument, buy_sell)

			# if position is a long position, check latest price against stop loss and take profit value
			if long_short == 'long':
				if stop_loss != 'na' and latest_price <= stop_loss:
					return stop_loss_return
				if take_profit != 'na' and latest_price >= take_profit:
					return take_profit_return

				return no_close_return
				
			# if position is a short position, check latest price against stop loss and take profit value
			elif long_short == 'short':
				if stop_loss != 'na' and latest_price >= stop_loss:
					return stop_loss_return
				if take_profit != 'na' and latest_price <= take_profit:
					return take_profit_return

				return no_close_return

			self.log('This should not happen, something went wrong in early closure!')
			raise Exception('Something went wrong in early closure!')

		return no_close_return

	def close_position(self, closure_reason):
		'''
		close an open position
		'''
		# verify that there is an open position
		if self.__holds_open_position():

			# get the prediction object
			prediction = db.read_prediction(self.position.prediction_id)


			if db.read_live_bit():
				# call the order api with the opposite amount that was ordered, and get the final closing price
				# Note: hard coded for oanda
				r = call_order_api(self.instrument, -s.trade_units * prediction.output)

				success, reason = verify_order(r)
				if not success: 
					self.log('Fatal')
					self.log(reason)
					return False

				close_price = float(r.json()['orderFillTransaction']['price'])

				# todo: double check with accountant that position has been closed

			else:
				# retrieve latest data
				self.__retrieve_latest_data().__compile_and_mine_data()
				close_price = float(self.latest_raw_data.iloc[self.n_candles-1]['close'])

			# update position on db
			time = ctime.from_now()
			self.position.open = False
			self.position.close_price = close_price
			self.position.closure_reason = closure_reason
			self.position.date_close = time.to_custom_date()
			self.position.datetime_close = str(time)
			self.position.profit_loss = (close_price - self.position.open_price) * self.position.amount
			db.update_position(self.position)

			# update merchant on db
			self.position = 'na'
			db.update_merchant(self, {'position_id': self.position})

			return True

		self.log('This should not happen, probably a race condition problem when checking for open position')
		return False

	def check_for_potential_opens(self):
		'''
		Check the prices and with predictor if there are any positions to be opened
		'''

		# retrieve latest data
		try:
			self.__retrieve_latest_data().__compile_and_mine_data()

			# make sure predictor is set
			if self.predictor == None: raise Exception("Predictor not set!")

			# pass latest data to predictor and get output and confidence
			sample = tensor(self.latest_processed_data.iloc[0].values).float()
			output, confidence = self.predictor.predict(sample)
			
			# flip, experimental
			# output = -output

			# if predictor predicts a possible position to be opened, save prediction to db
			if output != 0:

				# set buy/sell: if long position, we are looking to sell; if short position, we are looking to buy
				buy_sell = 'buy' if output == 1 else 'sell'

				# check if profit margin has been taken up in current candle
				# possible experiment: check price with floor and ceiling
				latest_price = call_get_price_api(self.instrument, buy_sell)
				allowable_open_price = self.latest_raw_data.iloc[-1]['close'] + (pip_to_currency(s.allowable_open_pips) * output)
				if buy_sell == 'buy':
					# if we are looking at a long position, latest price should not be greater than allowable price
					if latest_price > allowable_open_price:
						return False

				elif buy_sell == 'sell':
					# if we are lookg at a short position, latest price should not be lower than allowable price
					if latest_price < allowable_open_price:
						return False

				# make prediction and save to db
				datetime_of_last_candle = self.latest_raw_data.iloc[self.n_candles-1]['time']
				predicted_price = float(self.latest_raw_data.iloc[self.n_candles-1]['close']) + (pip_to_currency(s.target_pips) * output)
				prediction = Prediction(
					self.granularity_key,
					self.instrument,
					s.model_path,
					s.model_class,
					datetime_of_last_candle,
					str(ctime.from_now()),
					str(ctime(datetime_of_last_candle).add(*s.termination_offset)),
					predicted_price,
					PLATFORM,
					output,
					confidence,
				)

				# store prediction_id as latest prediction
				self.latest_prediction, _ = db.create_new_prediction(prediction)

			# todo: check current price deviation from wanted open price
			
			# officially, for there to be a potential open, 2 conditions must be met
			# 1. merchant must not hold any position, and
			# 2. predictor must have made a prediction
			if not self.__holds_open_position() and self.latest_prediction != None:
				return True

			return False
		except Exception as e:
			self.log(e)
			return False

	def open_position(self):
		'''
		opens a position
		'''
		# get the output for the latest prediction 
		latest_prediction = db.read_prediction(self.latest_prediction)
		latest_prediction_output = latest_prediction.output
		latest_prediction_confidence = latest_prediction.confidence

		# make sure latest_prediction output is valid
		if latest_prediction_output != 0:
			if db.read_live_bit():
				# make the official order 
				r = call_order_api(self.instrument, s.trade_units * latest_prediction_output) # todo: open position with stop loss

				success, reason = verify_order(r)
				if not success:
					self.log(reason)
					return False

				open_price = float(r.json()['orderFillTransaction']['price'])

			else:
				open_price = float(self.latest_raw_data.iloc[self.n_candles-1]['close'])

			# init long/short
			long_short = 'long' if latest_prediction_output > 0 else 'short'

			# eg: long position
			# open price = 1.500
			# stop loss = 1.500 - (1 + 10 pips) = 1.490
			# take profit = 1.500 + (1 + 15 pips) = 1.515
			#
			# eg: short position
			# open price 1.500
			# stop loss = 1.500 - (-1 + 10 pips) = 1.510
			# take profit = 1.500 + (-1 + 15 pips) = 1.485
			if s.stop_loss_pips != 'na':
				
				stop_loss = open_price - (latest_prediction_output * pip_to_currency(s.stop_loss_pips))
			else: 
				stop_loss = 'na'

			if s.take_profit_pips != 'na':
				take_profit = open_price + (latest_prediction_output * pip_to_currency(s.take_profit_pips))
			else:
				take_profit = 'na'

			# save position to db
			position = Position(
				self.granularity_key,
				self.instrument,
				s.trade_units * latest_prediction_output,
				long_short,
				open_price,
				str(ctime.from_now()),
				self.latest_prediction,
				PLATFORM,
				stop_loss,
				take_profit,
				confidence=latest_prediction_confidence
			)
			position_key, _ = db.create_new_position(position)

			# store the position
			self.position = db.read_position(position_key)

			# update the merchant
			db.update_merchant(self, {'position_id': position_key})

			return True

		self.log('Invalid prediction! (This should not happen!)')
		return False

	def __holds_open_position(self):
		# todo update check oanda if live position actually exists (or closed by stop loss)
		return not self.position == 'na' and not self.position == None

	def set_predictor(self, predictor):
		'''
		setter function for predictor
		'''
		self.predictor = predictor

	def set_accountant(self, accountant):
		self.accountant = accountant

	def get_latest_raw_data(self):
		'''
		getter function for latest raw data
		'''
		return self.latest_raw_data

	def get_latest_processed_data(self):
		'''
		getter function for latest processed data (post mining/feature extraction)
		'''
		return self.latest_processed_data

	def __retrieve_latest_data(self):
		'''
		Calls the candles api fom the respective platform api file. The candles retrieved will be the latest n candles, set by the n_candles parameter.
		The retrieved data will be put into a df and returned.

		Returns
		-----------
		df : DataFrame
			dataframe containing all the retrieve data
		'''
		r = call_get_candles_api(
			self.instrument,
			granularity=GRANULARITIES[self.granularity_key],
			count=self.n_candles+1
		)

		# todo: error handling
		# - check latest data datetime
		# - handle server errors
		# - check data completeness (last candle)
		try:
			data = r.json()['candles']
			data = [{
				'complete': datum['complete'],
				'volume': datum['volume'],
				'time': str(ctime.from_rfc3339(datum['time']).add(*self.temporal_offset)),
				'open': float(datum['mid']['o']),
				'high': float(datum['mid']['h']),
				'low': float(datum['mid']['l']),
				'close': float(datum['mid']['c'])
			} for datum in data]

			# make df
			df = pd.DataFrame.from_dict(data)

			# sort according to datetime
			df.sort_values(by=['time'], inplace=True)
			
			self.latest_raw_data = df

			return self
		except Exception as e:
			raise e

	def __compile_and_mine_data(self):
		'''
		compiles and mines the data in a format that is valid to make predictions on
		'''
		# compile the raw data into consecutive form
		df = compile_consecutive_data(self.latest_raw_data, self.n_candles, self.granularity_key)

		if len(df) != 2:
			raise Exception('Invalid/non consecutive data')
			
		# init mine object and perform corresponding mining
		mine = Mine(df, self.n_candles)
		mine.extract_body_height()
		mine.extract_upper_wick_height()
		mine.extract_lower_wick_height()
		mine.extract_all_increments_to_i_candle(self.n_candles-1)
		mine.drop_original_columns()
		self.latest_processed_data = mine.get_df()

		return self

	def log(self, message):
		l.log(self.get_info(), message)

	def get_info(self):
		return '{}-{}-{} merchant'.format(self.instrument, self.granularity_key, self.n_candles)

	def __str__(self):
		return '{}-{}-{}'.format(self.instrument, self.granularity_key, self.n_candles)

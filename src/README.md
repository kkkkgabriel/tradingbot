# Content/Code structure
- [data_collection](data_collection): scripts for collecting data
- [database](database): configurations and api for the database
- [deployment](deployment): Full system and configuration files for the trading bot
- [network_operations](network_operations): utility scripts for neural network operations, used in [deployment](deployment) and [training](training)
- [review](review): Scripts and notebooks for post deployment analysis
- [training](training): notebooks for compilation of data, training and testing
- [utils](utils): utility classes for all other folders to uses, notably [ctime.py](utils/ctime.py) (short for custom time) and [log.py](utils/log.py).


# Workflow
![This should load i guess.. not sure why it doesn't](../assets/workflow.png)     
Each of the above components of the workflow are briefly described as follows:    

1. **Data collection**    
Upon running functions in data_collection/collect.py, data will be stored in csv files in the data folder. The data will be stored in the following format data/{platform}/{granularity}/{date}/{currency_pair}.csv.
For more information, see the source codes in [data collection](data_collection/).    
&nbsp;
2. **Compilation**    
Once the raw data is collected, they have to be compiled in consecutive to become data points. For example, raw data for 12 consecutive candles can be compiled into 1 data point.
For more information, see [compile.py](network_operations/compile.py), hopefully the comments in the code is clear enough.    
&nbsp;
3. **Mining**    
By default, the compiled/raw data contains information for candlesticks: low, high, open, close. Features can be extracted from these data for better training, such as body height, wicks height and relative distance from one candle to another.
For more information, see [mine.py](network_operations/mine.py).    
&nbsp;
4. **Training**    
There are a few algorithms that are in the midst of experimentation, such as DBScan, iterative-Kmeans clustering, and neural networks. Pre and post training visualisations such as PCA, TSNE, ROC analysis and more are also included and implemented. See [training](training/) for more/.     
&nbsp;
5. **Testing**    
There are a couple of tests I do to determine how the models compare to each other, and decide which one to be deployed. Test can be done on daily/periodical data that contains multiple/single instruments. The models can also compilment each other to enhance precision, and such tests are also implemented. The test are implemented in notebooks in [training](training/).    
&nbsp;
6. **Deployment**    
The configurations for deployment are found in settings.py. The deployment system simulates a company that manages investment positions, through a fully OOP structure. See [deployment](deployment/) for more.    
&nbsp;
7. **Review**    
Despite the tests before a model is deployed, the performance of a model can only be definitely determined with profit. The review scripts breaks down the trades executed by the deployed system through heatmaps, candlesticks visualisation of trades and profit loss visualisation. See [review](review/) for more.   
  

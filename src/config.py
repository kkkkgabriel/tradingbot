import torch
import random
'''
global configurations
'''

#--------- seeds ------------#
RAND_SEED = 42
torch.manual_seed(RAND_SEED)
random.seed(RAND_SEED)

#--------- platform --------------#
PLATFORM = 'oanda'

#--------- data collection -----------#
DATA_FOLDER = '../../data/'

#--------- models ----------------#
MODEL_FOLDER = '../../models/'

#-------- logs -------#
DEV_LOGS_FOLDER = '../../logs/dev/'
PROD_LOGS_FOLDER = '../../logs/prod/'

# settable vars # todo: move to training folder
HYPERPARAMS = {
	"dataloader": {
		"params": {
			'batch_size': 32,
			'shuffle': False
		}
	}
}
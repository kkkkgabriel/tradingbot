import sys

sys.path.append('..')

from config import *
from utils.ctime import ctime
from utils.globals import *

from network_operations.compile import *
from network_operations.interpret import *
from network_operations.mine import *
from network_operations.netMixin import *
from network_operations.retrieval import *
from network_operations.networks import *
from network_operations.split import *
from network_operations.predict import *


# TODO: add conditions for import depending on platform
if PLATFORM == 'oanda':
	from oanda.api import *
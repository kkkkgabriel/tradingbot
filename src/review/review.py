from __init__ import *

import matplotlib.pyplot as plt
import pandas as pd
import plotly.graph_objects as go
import seaborn as sns

def plot_account_snapshots(account_snapshots):
    '''
    plot account snapshots
    '''
    if account_snapshots == None: 
        print('No account snapshots available')
        return

    # init vars
    timestamps, balances, margin_availables, margin_useds, profit_losses = [], [], [] ,[], []
    labels = ['balance', 'margin_available', 'margin_used', 'profit_loss']
    
    # collect content
    for timestamp, account in list(account_snapshots.items()):
        timestamps.append(timestamp)
        balances.append(account[labels[0]])
        margin_availables.append(account[labels[1]])
        margin_useds.append(account[labels[2]])
        profit_losses.append(account[labels[3]])
        
    # iteratively plot content
    content = [balances, margin_availables, margin_useds, profit_losses]
    for c, l in zip(content, labels):
        plt.plot(timestamps, c, label=l)
        plt.xticks(rotation=90)
        plt.legend()
        plt.show()

def plot_trades_heatmap(positions, sortation=['total'], cmap='cividis'):
    '''
    loop through positions to plot heatmap to see performance overview
    '''

    # loop through positions
    traded_instruments = {}
    for position in positions:
        instrument = position.instrument

        # make instrument in dictionary
        if instrument not in traded_instruments:
            traded_instruments[instrument] = {
                'total': 0,
                'bad': 0,
                'good': 0,
                'short':0,
                'good_short': 0,
                'bad_short': 0,
                'long':0,
                'good_long': 0,
                'bad_long': 0
            }

        # get stats
        target = traded_instruments[instrument]
        target['total'] += 1
        if position.long_short == 'long':
            target['long'] += 1
        else:
            target['short'] += 1
            
        if position.profit_loss > 0:
            target['good'] += 1
            
            if position.long_short == 'long':
                target['good_long'] += 1
            else:
                target['good_short'] += 1
            
        else:
            target['bad'] += 1
            
            if position.long_short == 'long':
                target['bad_long'] += 1
            else:
                target['bad_short'] += 1

    # make df with data
    df = pd.DataFrame.from_dict(traded_instruments, orient='index')
    df.sort_values(by=sortation, ascending=False, inplace=True)

    # plot heatmap
    sns.set(rc={'figure.figsize':(15, 15)})
    sns.heatmap(df, annot=True, cmap=cmap)

def plot_candles_of_position(position, ctime_duration=(105,'m')):
    '''
    plot candles of position

    Parameters
    ------------
    position : datebase.objects.Positions
        the position object to plot
    '''
    # extract trade details
    trade_details = position.to_json()

    # extract values
    instrument = trade_details['instrument']
    granularity = GRANULARITIES[trade_details['granularity_key']]
    long_short = trade_details['long_short']

    # parse time
    start = ctime(trade_details['datetime_open']).to_rfc3339_datetime()
    end = ctime(trade_details['datetime_open']).add(*ctime_duration).to_rfc3339_datetime()
    close_time = ctime(trade_details['datetime_close']).to_rfc3339_datetime()
    print(instrument, granularity, 'close: ', close_time, long_short)

    try:
        # retrieve candles from brokerage api
        r = call_get_candles_api(instrument, granularity=granularity, start=start, end=end)

        # parse candle data
        data = r.json()['candles']
        data = [{
            'complete': datum['complete'],
            'volume': datum['volume'],
            'time': ctime.from_rfc3339(datum['time']).add(8, 'h').to_rfc3339_datetime(),
            'open': datum['mid']['o'],
            'high': datum['mid']['h'],
            'low': datum['mid']['l'],
            'close': datum['mid']['c']
        } for datum in data]

        # make df
        df = pd.DataFrame.from_dict(data)
        fig = go.Figure(data=[go.Candlestick(x=df['time'],
                        open=df['open'],
                        high=df['high'],
                        low=df['low'],
                        close=df['close'])])

        # extract trade high and low
        trade_high = float(max(df.high))
        trade_low = float(min(df.low))
        
        # plot high, low, open and close price
        fig.add_hline(y=trade_high, annotation_text='trade_high')
        fig.add_hline(y=trade_low, annotation_text='trade_low')
        fig.add_hline(y=trade_details['open_price'], annotation_text='open_price')
        fig.add_hline(y=trade_details['close_price'], annotation_text='close_price')
        # fig.add_vline(x=trade_details['datetime_close'], annotation_text='close_time')

        # print out details
        print('amount: {}'.format(trade_details['amount']))
        print('open: {}'.format(trade_details['open_price']))
        print('close: {}'.format(trade_details['close_price']))
        print('pip change: {}'.format((trade_details['close_price'] - trade_details['open_price'])/0.0001))
        print('P&L: {}'.format(trade_details['profit_loss']))
        print('trade high: {}, {} pips'.format(trade_high, (trade_high-trade_details['open_price'])/0.0001))
        print('trade low: {}, {} pips'.format(trade_low, (trade_low-trade_details['open_price'])/0.0001))
        print('closure reason: {}'.format(trade_details['closure_reason']))
        print('score: {}'.format(trade_details['confidence']))

        # show 
        fig.show()
        return df
    except Exception as e:
        print('Incomplete data\n')
        

def sum_profit_loss(positions):
    '''
    sum profit losses given a list of positions

    Parameters 
    -----------
    positions : list
        a list of positions (objects.Position)
    '''
    if positions == None:
        print('No positions available')
        return
    total = 0
    for position in positions:
        if not position.open:
            total += position.profit_loss
    return total
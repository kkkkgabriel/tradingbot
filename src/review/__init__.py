import sys

sys.path.append('..')

from config import *
from utils.ctime import ctime
from utils.globals import *
from utils.log import Logger as l

from database.db_api import *
from database.objects import *

# TODO: add conditions for import depending on platform
if PLATFORM == 'oanda':
	from oanda.api import *
	from oanda._env import *
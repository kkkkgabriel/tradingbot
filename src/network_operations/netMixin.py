from __init__ import *
import torch
from torch.nn.utils import clip_grad_norm_
import numpy as np
from tqdm.auto import tqdm
from datetime import datetime
import torch.nn.functional as F

class NetMixin():
    def train_model(
        self,
        train_loader,
        val_loader,
        epochs,
        optimiser,
        loss_fn,
        device='cpu',
        print_every=1,
        save_every=-1,
        validate_every=1,
        save_location=MODEL_FOLDER + 'new_model.pt'
    ):
        # init array of losses to store
        losses = []

        # set model to device and set training mode
        self.to(device)
        self.train()

        # training loop
        for epoch in range(epochs):

            training_loss = 0
            training_size = []

            # print epoch nunmber and date time if necessary
            if print_every > 0 and (epoch + 1) % print_every == 0:
                now = datetime.now()
                print(f'---------- Epoch {epoch + 1} of {epochs} @ {now.strftime("%d-%m-%Y %H:%M:%S")} ----------')

            # get loss value from training data, and step the weights
            for (X, y) in tqdm(train_loader):
                X, y = X.to(device), y.to(device)
                
                # zero the gradients
                optimiser.zero_grad()
                
                # put the inputs through the model
                output = self.forward(X)
                
                y = y.view(-1, 1)
                y = y.float()
                
                # calculate loss
                loss = loss_fn(output, y)
                training_loss += loss.item()
                training_size.append(X.size(0))
                
                # backpropogation
                loss.backward()
                
                # optimize the model
                optimiser.step()

            # print training loss if necessary
            if print_every > 0 and (epoch + 1) % print_every == 0:
                now = datetime.now()
                training_samples = sum(training_size)/training_size[0]
                print(f'Average training loss: {training_loss / training_samples}')

            # validate model if necessary
            if validate_every > 0 and (epoch + 1) % validate_every == 0:
                val_loss = self.validate_model(val_loader, loss_fn, device)

                print("Average Validation Loss: {:.3f}".format(val_loss))
                losses.append((epoch, training_loss / training_samples, val_loss))

            # save model if necessary
            if save_every > 0 and (epoch + 1) % save_every == 0:
                self.save_model(save_location + '_epoch_{}'.format(epoch+1))

            print()

        # save final model in location
        self.save_model(save_location + '_epoch_{}'.format(epoch+1))

        return losses


    def validate_model(self, val_loader, loss_fn, device='cpu'):
        # set model to evaluation mode
        self.eval()

        training_loss = 0
        training_size = []

        with torch.no_grad(): # prevent weights from changing
            for (X, y) in tqdm(val_loader):
                X, y = X.to(device), y.to(device)
                output = self.forward(X)

                # compute average loss
                y = y.view(-1, 1)
                y = y.float()
                loss = loss_fn(output, y)
                training_loss += loss.item()
                training_size.append(X.size(0))

        self.train()
        training_samples = sum(training_size)/training_size[0]

        average_validation_loss = training_loss / training_samples
        return average_validation_loss

    def test_model(self, test_loader, threshold=0.5, device='cpu'):
        y_pred_list = []
        y_truth_list = []
        y_pred_probabilities_list = []

        self.eval()
        with torch.no_grad():
            for X, y in tqdm(test_loader):
                X = X.to(device)
                y_pred_probabilities = self(X).numpy()
                y_pred = np.where(y_pred_probabilities>threshold, 1, 0)
                y_pred_probabilities_list.append(y_pred_probabilities)
                y_pred_list.append(y_pred)
                y_truth_list.append(y)

        y_pred_probabilities_list = np.array([item for sublist in y_pred_probabilities_list for item in sublist])
        y_pred_list = np.array([item for sublist in y_pred_list for item in sublist])
        y_truth_list = np.array([item for sublist in y_truth_list for item in sublist])

        return y_truth_list, y_pred_list, y_pred_probabilities_list

    def save_model(self, path):
        torch.save(self.state_dict(), path)

    def load_model(self, path):
        self.load_state_dict(torch.load(path))
        self.eval()
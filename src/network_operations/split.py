#-------- split.py ----------#
from __init__ import *
from torch import from_numpy, tensor
from torch.utils.data import Dataset, DataLoader
from sklearn.model_selection import train_test_split

def split_df_into_sub_target_long_short(df, target_pips):
    amount = pip_to_currency(target_pips)
    sub_target_pips_df = df[abs(df['y']) < amount]
    long_target_pips_df = df[df['y'] > amount]
    short_target_pips_df = df[df['y'] < -amount]
    return sub_target_pips_df, long_target_pips_df,short_target_pips_df
 
def train_test_val_split(df):
    train_ratio, test_ratio, val_ratio = [i/sum(TRAIN_TEST_VAL_RATIO)for i in TRAIN_TEST_VAL_RATIO]
    train_test_df, val_df = train_test_split(df, test_size=val_ratio, random_state=RAND_SEED)
    train_df, test_df = train_test_split(train_test_df, test_size=test_ratio/sum([test_ratio, train_ratio]), random_state=RAND_SEED)
    return {
        'train': train_df,
        'test': test_df,
        'val': val_df
    }

def get_datasets(dfs):
    return {k: forex_dataset(v) for k, v in dfs.items()}

def get_dataloaders(datasets):
    return {k: DataLoader(v, **HYPERPARAMS['dataloader']['params']) for k, v in datasets.items()}

class forex_dataset(Dataset):
    def __init__(self, df, y_header='y'):
        self.df = df
        self.y_header = y_header
        self.x_header = list(df.columns)
        self.x_header.remove(y_header)
    
    def __len__(self):
        return self.df.shape[0]
    
    def __getitem__(self, idx):
        data = from_numpy(self.df[self.x_header].iloc[idx].values).float()
        label = tensor(self.df[self.y_header].iloc[idx])
        
        return data, label
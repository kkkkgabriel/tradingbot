from __init__ import *
import torch
import torch.nn as nn
from network_operations.netMixin import NetMixin

class positive_linear_v1(nn.Module, NetMixin):
    def __init__(self):
        super(positive_linear_v1, self).__init__()
        self.linear_relu_stack = nn.Sequential(
            nn.Linear(19, 30),
            nn.Linear(30, 50),
            nn.Linear(50, 100),
            nn.Linear(100, 100),
            nn.Linear(100, 100),
            nn.Linear(100, 50),
            nn.Linear(50, 30),
            nn.Linear(30, 19),
            nn.Linear(19, 1),
            nn.Sigmoid()
        )

    def forward(self, x):
        logits = self.linear_relu_stack(x)
        return logits

class negative_linear_v1(nn.Module, NetMixin):
    def __init__(self):
        super(negative_linear_v1, self).__init__()
        self.linear_relu_stack = nn.Sequential(
            nn.Linear(19, 30),
            nn.Linear(30, 50),
            nn.Linear(50, 100),
            nn.Linear(100, 100),
            nn.Linear(100, 100),
            nn.Linear(100, 50),
            nn.Linear(50, 30),
            nn.Linear(30, 19),
            nn.Linear(19, 1),
            nn.Sigmoid()
        )

    def forward(self, x):
        logits = self.linear_relu_stack(x)
        return logits

class non_bin_v2(nn.Module, NetMixin):
    def __init__(self):
        super(non_bin_v2, self).__init__()
        self.linear_relu_stack = nn.Sequential(
            nn.Linear(19, 50),
            nn.LeakyReLU(0.1, inplace=True),
            nn.Linear(50, 200),
            nn.LeakyReLU(0.1, inplace=True),
            nn.Linear(200, 200),
            nn.LeakyReLU(0.1, inplace=True),
            nn.Linear(200, 100),
            nn.LeakyReLU(0.1, inplace=True),
            nn.Linear(100, 50),
            nn.LeakyReLU(0.1, inplace=True),
            nn.Linear(50, 1),
            nn.Sigmoid()
        )

    def forward(self, x):
        logits = self.linear_relu_stack(x)
        return logits

class non_bin_v3(nn.Module, NetMixin):
    def __init__(self):
        super(non_bin_v3, self).__init__()
        self.linear_relu_stack = nn.Sequential(
            nn.Linear(19, 100),
            nn.LeakyReLU(0.1, inplace=True),
            nn.Linear(100, 200),
            nn.LeakyReLU(0.1, inplace=True),
            nn.Linear(200, 200),
            nn.LeakyReLU(0.1, inplace=True),
            nn.Linear(200, 200),
            nn.LeakyReLU(0.1, inplace=True),
            nn.Linear(200, 50),
            nn.LeakyReLU(0.1, inplace=True),
            nn.Linear(50, 1),
            nn.Sigmoid()
        )

    def forward(self, x):
        logits = self.linear_relu_stack(x)
        return logits







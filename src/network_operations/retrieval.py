from __init__ import *
import pandas as pd

'''
This script contains function to retrieve data from the saved csvs in a dataframe.
'''

def retrieve_particular_csv(granularity_key, date, instrument, verbose=True):
	'''
	retrieves data from one particular csv

	Parameters
	-----------
	granularity_key : str
		The key of the granularity defined in the respective platform _env file
	date : str
		date of the data to be retrieved in custom datetime format, eg: 20210808000000
	instrument : str
		One of the instruments defined in the respective platform _env file
	verbose : bool (optional)
		prints out log messages if true

	Returns
	-----------
	df : pd.DataFrame
		dataframe containing all the data
	'''
	# print log message
	if verbose: print('retrieving {} {} on {}.'.format(instrument, granularity_key, date))

	# parse date 
	date = ctime(date).to_rfc3339_date()

	# set path a retrieve data
	path = DATA_FOLDER + PLATFORM + '/' + granularity_key + '/' + date + '/' + instrument +'.csv'
	try:
		df = pd.read_csv(path)
		del df['Unnamed: 0']
		return df
	except Exception as e:
		if verbose: print('\tFailed....')
		raise e

def retrieve_csv_for_day_for_instruments(granularity_key, date, instruments, verbose=True):
	'''
	retrieves the data from multiple csvs across multiple instruments on a single date

	Parameters
	-----------
	granularity_key : str
		The key of the granularity defined in the respective platform _env file
	date : str
		date of the date to retrieved in custom datetime format, eg: 20211010080000 (YYYYMMDDhhmmss)
	instruments : list
		a list of instruments to retrieve
	verbose : bool (optional, default to true)
		prints out log messages if true

	'''
	# parse date
	date = ctime(date)

	# init vars
	invalid_count = 0
	dfs = []

	# loop through instruments and retrieve csv for that instrument on that date
	for instrument in instruments:
		try:
			# retrieve data from date
			df = retrieve_particular_csv(granularity_key, date.to_custom_datetime(), instrument, verbose=verbose)

			# check data validity; data is considered invalid number of rows is not the same as specificied in LENGTH_IN_DAY in _env
			if df.shape[0] == LENGTH_IN_DAY[granularity_key]:
				dfs.append(df)
			else:
				raise Exception('Incorrect length')

		except Exception as e:
			if verbose: print('\tIncomplete/Invalid data for {} {}, on {}'.format(instrument, granularity_key, date.to_custom_date()))
			invalid_count += 1 

	if verbose: print('Compiled {} days of data, with {} days invalid'.format(len(dfs), invalid_count))
	
	if len(dfs) == 0:
		raise Exception('No valid data')
	else:
		return pd.concat(dfs, ignore_index=True)

def retrieve_csv_for_period(granularity_key, start_date, end_date, instrument, verbose=True):
	'''
	retrieves data from multiple particular csv between start_date and end_date, concatenates them into one dataframe.

	Parameters
	-----------
	granularity_key : str
		The key of the granularity defined in the respective platform _env file
	start_date : str
		start date of the data to be retrieved in custom datetime format, eg: 20210808000000
	end_date : str
		end date of the data to be retrieved in custom datetime format, eg: 20210808000000
	instrument : str
		One of the instruments defined in the respective platform _env file
	verbose : bool (optional)
		prints out log messages if true

	Returns
	-----------
	df : pd.DataFrame
		dataframe containing all the data
	'''
	# init ctime object for start and end date
	start = ctime(start_date)
	end = ctime(end_date)
	
	# init vars
	invalid_count = 0 # to store number of invalid csvs that
	dfs = [] # list to store all dfs
	
	# loop dates from start to end
	while start < end:
		try:
			# retrieve data from date
			df = retrieve_particular_csv(granularity_key, start.to_custom_datetime(), instrument, verbose=verbose)

			# check data validity; data is considered invalid number of rows is not the same as specificied in LENGTH_IN_DAY in _env
			if df.shape[0] == LENGTH_IN_DAY[granularity_key]:
				dfs.append(df)
			else:
				raise Exception('Incorrect length')

		except Exception as e:
			if verbose: print('\tIncomplete/Invalid data for {} {}, on {}'.format(instrument, granularity_key, start.to_custom_date()))
			invalid_count += 1 

		# increment start
		start.add(1, 'D')

	if verbose: print('Compiled {} days of data, with {} days invalid'.format(len(dfs), invalid_count))
	
	if len(dfs) == 0:
		raise Exception('No valid data')
	else:
		return pd.concat(dfs, ignore_index=True)

def retrieve_csv_for_period_for_instruments(granularity_key, start_date, end_date, instruments, verbose=True):
	'''
	retrieves the data from multiple csvs across multiple instruments on multiple dates

	Parameters
	-----------
	granularity_key : str
		The key of the granularity defined in the respective platform _env file
	start_date : str
		start date of the date to retrieved in custom datetime format, eg: 20211010080000 (YYYYMMDDhhmmss)
	end_date : str
		end date of the date to retrieved in custom datetime format, eg: 20211010080000 (YYYYMMDDhhmmss)
	instruments : list
		a list of instruments to retrieve
	verbose : bool (optional, default to true)
		prints out log messages if true

	'''
	# parse dates
	start = ctime(start_date)
	end = ctime(end_date)

	# init vars
	invalid_count = 0
	dfs = []

	# loop through start date until it reaches end date
	while start < end:
		for instrument in instruments:
			try:
				# retrieve data from date
				df = retrieve_particular_csv(granularity_key, start.to_custom_datetime(), instrument, verbose=verbose)

				# check data validity; data is considered invalid number of rows is not the same as specificied in LENGTH_IN_DAY in _env
				if df.shape[0] == LENGTH_IN_DAY[granularity_key]:
					dfs.append(df)
				else:
					raise Exception('Incorrect length')
			except Exception as e:
				if verbose: print('\tIncomplete/Invalid data for {} {}, on {}'.format(instrument, granularity_key, start.to_custom_date()))
				invalid_count += 1 
		
		# increment start
		start.add(1, 'D')

	if verbose: print('Compiled {} days of data, with {} days invalid'.format(len(dfs), invalid_count))
	
	if len(dfs) == 0:
		raise Exception('No valid data')
	else:
		return pd.concat(dfs, ignore_index=True)

def retrieve_csv_for_month(granularity_key, start_date, instrument,verbose=True):
	'''
	retrieves data from multiple particular csv within a specified month, concatenates them into one dataframe.

	Parameters
	-----------
	granularity_key : str
		The key of the granularity defined in the respective platform _env file
	start_date : str
		start date of the month to be retrieved in custom datetime format, eg: 20210800000000
	instrument : str
		One of the instruments defined in the respective platform _env file
	verbose : bool (optional)
		prints out log messages if true

	Returns
	-----------
	df : pd.DataFrame
		dataframe containing all the data
	'''
	# parse start and end times
	start = ctime(start_date).to_custom_datetime()
	end = ctime(start_date).add(1, 'M').to_custom_datetime()
	
	return retrieve_csv_for_period(granularity_key, start, end, instrument, verbose=verbose)


def __test():
	print('---test 1---')
	granularity_key = '1_minute'
	start = '20210801000000' # rfc3339
	instrument = INSTRUMENTS[0]
	print(retrieve_particular_csv(granularity_key, start, instrument))

	print('---test 2---')
	granularity_key = '15_minute'
	start = '20210801000000'
	end = '20210901000000'
	instrument = INSTRUMENTS[0]
	print(retrieve_csv_for_period(granularity_key, start, end, instrument))

	print('---test 3---')
	granularity_key = '15_minute'
	start = '20210801000000'
	instrument = INSTRUMENTS[0]
	print(retrieve_csv_for_month(granularity_key, start, instrument))
	
if __name__ == '__main__':
	__test()
from __init__ import *
import pandas as pd
import numpy as np
from copy import deepcopy

class Mine_v2:
    def __init__(self, original_df, consecutive, n_candles_as_x, take_profit_pips, stop_loss_pips):
        self.o_df = original_df
        self.consecutive = consecutive
        self.n_candles_as_x = n_candles_as_x
        self.take_profit_amount = pip_to_currency(take_profit_pips)
        self.stop_loss_amount = pip_to_currency(stop_loss_pips)
        self.df = deepcopy(original_df)
        self.intermediate_labels = []

    def extract_high_score(self, high_score_increments=[2,2,2,1,1,1,1], high_score_multiplier=[0, 0, 0, 0.5, 0.7, 0.8, 0.9], label='high_score', type='increment'):
        df = self.df
        
        if type == 'increment':
            def extract_high_score_from_row(row):
                total_score = 0
                
                # get the mid price of the last input candle (idx=4)
                mid_of_last_input_candle = sum([row['close{}'.format(self.n_candles_as_x-1)], row['open{}'.format(self.n_candles_as_x-1)]])/2
                
                # calc take profit and stop loss price
                take_profit_price = mid_of_last_input_candle + self.take_profit_amount
                
                for n, i in zip(range(self.n_candles_as_x, self.consecutive), high_score_increments):
                    # incentivise if high is greater than take profit
                    if row['high{}'.format(n)] >= take_profit_price:
                        total_score += i
                return total_score

        elif type == 'multiplier':

            def extract_high_score_from_row(row):
                total_score = 1
                
                # get the mid price of the last input candle (idx=4)
                mid_of_last_input_candle = sum([row['close{}'.format(self.n_candles_as_x-1)], row['open{}'.format(self.n_candles_as_x-1)]])/2
                
                # calc take profit and stop loss price
                stop_loss_price = mid_of_last_input_candle + self.stop_loss_amount
                
                for n, i in zip(range(self.n_candles_as_x, self.consecutive), high_score_multiplier):
                    # punish if high is greater than stop loss
                    if row['high{}'.format(n)] >= stop_loss_price:
                        total_score *= i
                return total_score
        
        else: raise Exception('Invalid type!')

        df[label] = df.apply(extract_high_score_from_row, axis=1)
        self.intermediate_labels.append(label)
        
    def extract_close_score(self, close_score_increments=[-2, -2, -2, -1, -1, -1, -1], label='close_score', close_score_pips=-10, close_type='floor'):

        df = self.df
        close_score_amount = pip_to_currency(close_score_pips)
        
        def extract_close_score_from_row(row):
            total_score = abs(sum(close_score_increments))
            
            # get the mid price of the last input candle (idx=4)
            mid_of_last_input_candle = sum([row['close{}'.format(self.n_candles_as_x-1)], row['open{}'.format(self.n_candles_as_x-1)]])/2
            
            # calc lowest close_score price acceptable
            close_score_price = mid_of_last_input_candle + close_score_amount
            
            for n, i in zip(range(self.n_candles_as_x, self.consecutive), close_score_increments):

                if close_type == 'floor':
                    # if close price is a floor, punish if close is lower than floor price
                    if row['close{}'.format(n)] <= close_score_price:
                        total_score += i

                elif close_type == 'ceiling':
                    # if close price is a ceiling, punish if close is higher than ceiling
                    if row['close{}'.format(n)] >= close_score_price:
                        total_score += i

            return total_score
            
        df[label] = df.apply(extract_close_score_from_row, axis=1)
        self.intermediate_labels.append(label)
        
    def extract_low_score(self, low_score_increments=[2,2,2,1,1,1,1], low_score_multiplier=[0, 0, 0, 0.5, 0.7, 0.8, 0.9], label='low_score', type='multiplier'):
        df = self.df
        
        if type == 'multiplier':
            def extract_low_score_from_row(row):
                total_score = 1
                
                # get the mid price of the last input candle (idx=4)
                mid_of_last_input_candle = sum([row['close{}'.format(self.n_candles_as_x-1)], row['open{}'.format(self.n_candles_as_x-1)]])/2
                
                # calc stop loss 
                stop_loss_price = mid_of_last_input_candle + self.stop_loss_amount
                
                for n, i in zip(range(self.n_candles_as_x, self.consecutive), low_score_multiplier):
                    # punish is low is lower than stop loss
                    if row['low{}'.format(n)] <= stop_loss_price:
                        total_score *= i
                        
                return total_score

        elif type == 'increment':

            def extract_low_score_from_row(row):
                total_score = 0
                
                # get the mid price of the last input candle (idx=4)
                mid_of_last_input_candle = sum([row['close{}'.format(self.n_candles_as_x-1)], row['open{}'.format(self.n_candles_as_x-1)]])/2
                
                # calc take profit
                take_profit_price = mid_of_last_input_candle + self.take_profit_amount
                
                for n, i in zip(range(self.n_candles_as_x, self.consecutive), low_score_increments):
                    # incentivise if low is lower than take profit
                    if row['low{}'.format(n)] <= take_profit_price:
                        total_score += i
                        
                return total_score

        df[label] = df.apply(extract_low_score_from_row, axis=1)
        self.intermediate_labels.append(label)
        
    def extract_compound_score(
        self,
        label='y',
        increment_label='high_score',
        close_score_label='close_score',
        multipler_label='low_score'
    ):
        df = self.df
        
        def extract_compound_score_from_row(row):
            weighted_score = (sum([row[increment_label] + row[close_score_label]])/2)
            score = weighted_score * 0.1 * row[multipler_label]
            return score
        
        df[label] = df.apply(extract_compound_score_from_row, axis=1)
        
        
    def drop_intermediate_headers(self):
        df = self.df
        for label in self.intermediate_labels:
            try:
                del df[label]
            except:
                pass
            
    def drop_original_headers(self):
        df = self.df
        original_headers = self.o_df.columns
        for header in original_headers:
            try:
                del df[header]
            except:
                pass
            
    def drop_headers(self, headers):
        df = self.df
        for header in headers:
            try:
                del df[header]
            except:
                pass
            
    def add_binary_label_to_o_df(self, label='y'):
        self.o_df[label] = self.df[label]
        
    def extract_body_height(self):
        '''
        Creates columns 'body_height0'... 'body_heightN-1' where N is consecutive to store the height of each candle.
        The height of each candle is defined as close - open.
        '''
        df = self.df
        for i in range(self.consecutive):
            df['body_height{}'.format(i)] = df['close{}'.format(i)] - df['open{}'.format(i)]
        return self
    
    def extract_upper_wick_height(self):
        '''
        Creates columns 'upper_wick0'... 'upper_wickN-1' where N is consecutive to store the upper wick of each candle.
        The upper wick will be positive for green (bullish) candles and negative for red (bearish) candles.
        '''
        df = self.df
        for i in range(self.consecutive):
            df['upper_wick{}'.format(i)] =  np.where(
                df['body_height{}'.format(i)] > 0,
                df['high{}'.format(i)] - df['close{}'.format(i)],
                df['open{}'.format(i)] - df['high{}'.format(i)]
            )
        return self
    
    def extract_lower_wick_height(self):
        '''
        Creates columns 'lower_wick0'... 'lower_wickN-1' where N is consecutive to store the upper wick of each candle.
        The lower wick will be positive for green (bullish) candles and negative for red (bearish) candles.
        '''
        df = self.df
        for i in range(self.consecutive):
            df['lower_wick{}'.format(i)] =  np.where(
                df['body_height{}'.format(i)] > 0,
                df['open{}'.format(i)] - df['low{}'.format(i)],
                df['low{}'.format(i)] - df['close{}'.format(i)]
            )
        return self
    
    def extract_all_increments_to_i_candle(self):
        '''
        Creates columns 'increment0'... 'incrementI-1'.
        The increment is defined as each candle's open to the last candle's close

        Parameters
        -----------
        i : int
            The index of the ith candle to get the increments to
        '''
        i = self.n_candles_as_x-1
        df = self.df
        if i >= self.consecutive:
            print('Value of I (index) ({}) exceeded value of consecutive ({})'.format(i, self.consecutive))
            print('Setting i to the last candle\'s index: {}'.format(self.consecutive - 1))
            i = self.consecutive - 1

        for n in range(i):
            df['increment_to_{}_from{}'.format(i, n)] = df['close{}'.format(n)] - df['close{}'.format(i)]
        return self

'''
mine performs mining operations on the raw dataset to extract features
'''
class Mine:
    def __init__(self, original_df, consecutive):
        '''
        Initialises a mine object

        Parameters 
        -----------
        original_df : pd.DataFrame
            raw dataset, from compile_consecutive_data in compile.py
        consecutive : int
            consecutive number of candles that dataset contains
        '''
        self.df = original_df
        self.consecutive = consecutive
        self.original_headers = deepcopy(self.df.columns)

        # mined_candles_headers to store new columns that have been created for all candles
        self.mined_candle_headers = []

        # mined_headers store new columns that may or may not correspond to a single or multiple candles
        self.mined_headers = []

    def extract_body_height(self):
        '''
        Creates columns 'body_height0'... 'body_heightN-1' where N is consecutive to store the height of each candle.
        The height of each candle is defined as close - open.
        '''
        df = self.df
        for i in range(self.consecutive):
            df['body_height{}'.format(i)] = df['close{}'.format(i)] - df['open{}'.format(i)]
        self.mined_candle_headers.append('body_height')
        return self
    
    def extract_upper_wick_height(self):
        '''
        Creates columns 'upper_wick0'... 'upper_wickN-1' where N is consecutive to store the upper wick of each candle.
        The upper wick will be positive for green (bullish) candles and negative for red (bearish) candles.
        '''
        df = self.df
        for i in range(self.consecutive):
            df['upper_wick{}'.format(i)] =  np.where(
                df['body_height{}'.format(i)] > 0,
                df['high{}'.format(i)] - df['close{}'.format(i)],
                df['open{}'.format(i)] - df['high{}'.format(i)]
            )
        self.mined_candle_headers.append('upper_wick')
        return self
    
    def extract_lower_wick_height(self):
        '''
        Creates columns 'lower_wick0'... 'lower_wickN-1' where N is consecutive to store the upper wick of each candle.
        The lower wick will be positive for green (bullish) candles and negative for red (bearish) candles.
        '''
        df = self.df
        for i in range(self.consecutive):
            df['lower_wick{}'.format(i)] =  np.where(
                df['body_height{}'.format(i)] > 0,
                df['open{}'.format(i)] - df['low{}'.format(i)],
                df['low{}'.format(i)] - df['close{}'.format(i)]
            )
        self.mined_candle_headers.append('lower_wick')
        return self
    
    def extract_all_increments_to_last_candle(self):
        '''
        Creates columns 'increment0'... 'incrementN-1' where N is consecutive to store the upper wick of each candle.
        The increment is defined as each candle's open to the last candle's close
        '''
        df = self.df
        for i in reversed(range(self.consecutive)):
            df['increment{}'.format(i)] = df['close{}'.format(self.consecutive-1)] - df['open{}'.format(i)]
        self.mined_candle_headers.append('increment')
        return self
    
    def extract_increment_from_i_to_last_candle(self, i, header='y'):
        '''
        Creates column {header} to store the increment from the ith candle to the last.
        The increment is defined as the ith candle's open to the last candle's close.

        Parameters 
        -----------
        i : int
            the ith candle to get the increment from
        header : str (optional)
            the header name to the new column
        '''
        df = self.df
        df[header] = df['close{}'.format(self.consecutive-1)] - df['open{}'.format(i)]
        self.mined_headers.append(header)
        return self

    def extract_all_increments_to_i_candle(self, i):
        '''
        Creates columns 'increment0'... 'incrementI-1'.
        The increment is defined as each candle's open to the last candle's close

        Parameters
        -----------
        i : int
            The index of the ith candle to get the increments to
        '''
        df = self.df
        if i >= self.consecutive:
            print('Value of I (index) ({}) exceeded value of consecutive ({})'.format(i, self.consecutive))
            print('Setting i to the last candle\'s index: {}'.format(self.consecutive - 1))
            i = self.consecutive - 1

        for n in range(i):
            df['increment_to_{}_from{}'.format(i, n)] = df['close{}'.format(n)] - df['close{}'.format(i)]
            self.mined_headers.append('increment_to_{}_from{}'.format(i, n))
        return self

    def extract_binary_targets(self, target_pips, header='y', increment_col='increment'):
        '''
        Creates column {header} to store binary targets depending if value in {increment_col} is above target_pips

        Parameters
        -----------
        target_pips : int
            number of pips to filter as target
        header : str (optional)
            the header for the new column
        increment_col : str (optional)
            the header of the column to check, default to 'increment'
        '''
        df = self.df
        amount = pip_to_currency(target_pips)
        if target_pips > 0:
            df[header] = df[increment_col].apply(lambda x: 1.0 if x > amount else 0.0)
        else:
            df[header] = df[increment_col].apply(lambda x: 1.0 if x < amount else 0.0)
        return self

    def extract_yin_yang_targets(self, yin_target, yang_target, header='y', increment_col='increment'):
        df = self.df

        yin_amount = pip_to_currency(yin_target)
        yang_amount = pip_to_currency(yang_target)

        def yin_yang_cmp(x):
            if x > yang_amount: return 1.0
            elif x < yin_amount: return -1.0
            else: return 0        

        df[header] = df[increment_col].apply(yin_yang_cmp)
        return self

    def drop_columns(self, columns):
        '''
        Drops columns

        Parameters 
        -----------
        columns : list
            list of column names to drop
        '''
        df = self.df
        for col in columns:
            try:
                del df[col]
            except Exception as e:
                pass
        return self

    def drop_original_columns(self):
        '''
        Drops original columns
        '''
        df = self.df
        for col in self.original_headers:
            try: 
                del df['{}'.format(col)]
            except Exception as e:
                print(e)
        return self

    def drop_selected_candle_features(self, col_indexes):
        '''
        Drops selected candles features that have been extracted for all
        '''
        df = self.df
        for i in col_indexes:
            for header in self.mined_candle_headers:
                del df['{}{}'.format(header, i)]
        return self
        
    def get_df(self):
        '''
        Retrieved mined dataframe

        Returns
        -----------
        : pd.DataFrame
            mined dataframe
        '''
        return self.df
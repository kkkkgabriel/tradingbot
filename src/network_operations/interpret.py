import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import confusion_matrix, roc_auc_score, roc_curve, ConfusionMatrixDisplay

'''
This script contains function to interpret the results of a model.
'''
def unpack_losses(losses):
    '''
    Unpacks losses (from netMixin.py's train_model function) into respective lists 

    Parameters
    -----------
    losses : 2d list
        Eg: [
                [{index}, {train_loss}, {val_loss}],
                [0, 0.5, 0.1],
                ...
            ]
    
    Returns
    -----------
    indexes : list (of int)
        list of index/epochs
    train_loss : list (of float)
        list of training losses corresponding to the epoch
    val_loss : list (of float)
        list of validation losses corresponding to the epoch
    '''
    losses = np.array(losses)
    indexes = losses[:, 0]
    train_loss = losses[:,1]
    val_loss = losses[:,2]
    return indexes, train_loss, val_loss

def plot(indexes, losses, labels):
    '''
    Plot the losses onto one graph

    Parameters
    -----------
    indexes : list (of int)
        x values
    losses : list (of list (of float))
        list of y values to be plotted
    labels : list
        list of labels corresponding to the values in losses
    '''
    for loss, label in zip(losses, labels):
        plt.plot(indexes, loss, label=label)
    plt.legend()
    plt.show()

def get_confusion_matrix(truth, predictions, labels, verbose=True):
    '''
    Get tn, tp, fn, tp (values of the confusion matrix)

    Parameters
    -----------
    truth : np.array
        truth values
    predictions : np.array
        prediction values
    labels : list
        labels in truth and predictions
    verbose : bool (optional)
        if true, prints out confusion matrix 

    Returns
    -----------
    tn : int
        true negative count
    fp : int
        false positive count
    fn : int
        false negative count
    tp : int
        true positive count
    '''
    cm = confusion_matrix(truth, predictions, labels=labels)
    
    if verbose:
        disp = ConfusionMatrixDisplay(confusion_matrix=cm, display_labels=labels)
        disp.plot()
        plt.show()
        
    tn, fp, fn, tp = cm.ravel()

    return tn, fp, fn, tp

def plot_precision_recall_against_threshold(y_truth_list, y_pred_probabilities_list):
    '''
    for threshold values of 0 to 1 with increments of 0.01, get precision and recalls, and plot them on a graph

    Parameters
    -----------
    y_truth_list : list
        a list of truth values
    y_pred_probabilities_list : list
        a list of predicted probabilities
    '''
    # init lists
    recalls = []
    thresholds = []
    precisions = []

    # loop i from 0 to 100
    for i in range(0, 100, 1):
        threshold = i/100
        
        # make predictions by comparing probabilities list to threshold value
        pred = np.where(y_pred_probabilities_list>threshold, 1.0, 0.0)
        pred = np.squeeze(pred)
        
        # calculate recall
        positives_prediction = pred[y_truth_list.nonzero()].sum()
        positives = y_truth_list[y_truth_list.nonzero()].sum()
        recall = positives_prediction/positives
        
        # calculate precision
        predicted_positives = pred[pred.nonzero()].sum()
        if predicted_positives == 0:
            precision = 0
        else:
            predicted_actual = y_truth_list[pred.nonzero()].sum()
            precision = predicted_actual/predicted_positives
        
        # add to list
        thresholds.append(threshold)
        recalls.append(recall)
        precisions.append(precision)

    # plot values on graph
    plt.plot(thresholds, recalls, label='Recall')
    plt.plot(thresholds, precisions, label='Precision')
    plt.legend()
    plt.show()

def metricify(tn, fp, fn, tp, verbose=True):
    '''
    Calculate recall, precision, f1 and accuracy

    Parameters
    ----------
    tn : int
        true negative count
    fp : int
        false positive count
    fn : int
        false negative count
    tp : int
        true positive count
    verbose : bool (optional)
        if true, prints out the values

    Returns
    ----------
    recall : float
    precision : float
    f1 : float
    accuracy : float
    '''
    # math
    recall = tp/(tp+fn)
    precision = tp/(tp+fp)
    f1 = 2 * (precision * recall) / (precision + recall)
    accuracy = (tp+tn)/(tp+tn+fp+fn)

    # print scores if verbose
    if verbose:
        print('Recall: {}'.format(recall))
        print('Precision: {}'.format(precision))
        print('F1: {}'.format(f1))
        print('Accuracy: {}'.format(accuracy))

    return recall, precision, f1, accuracy

def get_optimal_threshold(y_truth_list, y_pred_probabilities_list, verbose=True):
    '''
    Get the optimal threshold value to maximise the f1 score 

    Parameters
    -----------
    y_truth_list : np.array
        truth values
    y_pred_probabilities_list : np.array
        prediction probabilities
    verbose : bool (optional)
        if true, plots out roc curve

    Returns
    -----------
    optimal_threshold : float
        optimal_threshold to maximise tpr-fpr, maximising f1
    '''
    # get roc auc score
    roc_auc = roc_auc_score(y_truth_list, y_pred_probabilities_list)

    # get fpr tpr values for corresponding thresholds
    fpr, tpr, thresholds = roc_curve(y_truth_list, y_pred_probabilities_list)

    # plot roc curve if verbose
    if verbose:
        plt.figure(1)
        plt.plot([0, 1], [0, 1])
        plt.plot(fpr, tpr, label='CNN(area = {:.3f})'.format(roc_auc))
        plt.xlabel('False positive rate')
        plt.ylabel('True positive rate')
        plt.title('ROC curve')
        plt.legend(loc='best')
        plt.show()

    # get optimal threshold
    optimal_idx = np.argmax(tpr - fpr)
    optimal_threshold = thresholds[optimal_idx]

    return optimal_threshold
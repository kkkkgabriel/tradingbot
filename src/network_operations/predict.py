from torch import tensor

'''
Predictor is a parent class for child classes to inherit. This inheritance allows the predictor to apply predictions and get prediction probabilities to dataframes
Predictors have to implement the predict and predict_proba function.
'''
class Predictor():
    def apply_predictions_to_df(self, row):
        sample = tensor(row.values[0:self.n_inputs]).float()
        prediction = self.predict(sample)
        return prediction

    def apply_predictions_proba_to_df(self, row):
        sample = tensor(row.values[0:self.n_inputs]).float()
        prediction_proba = self.predict_proba(sample)
        return prediction_proba

'''
A prophet object makes a binary classification with a threshold value
'''
class Prophet(Predictor):
    def __init__(self, model, n_inputs, threshold=0.5):
        '''
        Initialises a prophet object

        Parameters
        -----------
        model : nn.Module
            model to make predictions with (implemented in pytorch)
        n_inputs : int
            number of inputs that the model expects
        threshold : float (optional, default to 0.5)
            threshold value to make a positive prediction
        '''
        self.model = model
        self.threshold = threshold
        self.n_inputs = n_inputs
        
    @classmethod
    def load_model(prophet, model_class, model_path, n_inputs, threshold=0.5):
        '''
        inits a prophet object along with a model

        Parameters
        -----------
        model_class : class
            the model class to be created
        model_path : str
            the location of the weights of that model
        n_inputs : int
            number of inputs that the model expects
        threshold : float (optional, default to 0.5)
            threshold value to make a positive prediction
        '''
        # init the model
        model = model_class()

        # load model weights
        model.load_model(model_path)

        # init prophet
        return prophet(model, n_inputs, threshold=threshold)
        
    def predict(self, sample):
        '''
        predict

        Parameters
        -----------
        sample : np.array
            the sample value to make a prediction on, it should have the shape of n_inputs

        Returns
        -----------
        : int
            1 or 0 (binary classification duh)
        output.item() : float
            score that the model returned
        '''

        # init model and set to evaluation mode
        model = self.model
        model.eval()

        # put sample through model
        output = model(sample)

        # check if output is greater than threshold
        if output > self.threshold:
            return 1, output.item()
        else:
            return 0, output.item()

    def predict_proba(self, sample):
        '''
        get probability score

        Parameters
        -----------
        sample : np.array
            the sample value to make a prediction on, it should ahve the shape of n_inputs

        Returns
        -----------
        proba : float
            score that the model returned
        '''
        model = self.model
        
        model.eval()
        proba = model(sample).item()
        return proba

'''
A yin_yang_prophets object makes a 3 class prediction (-1,0,1), using 2 prophet objects working in opposite.
'''
class Yin_yang_prophets(Predictor):
    def __init__(
        self,
        yin_model,
        yang_model,
        n_inputs,
        yin_threshold=0.5,
        yang_threshold=0.5,
        yin_ceiling_threshold=1,
        yang_ceiling_threshold=1
    ):
        '''
        Initialises a Yin_yang_prophets object

        Parameters
        -----------
        yin_model : nn.Module
            model to make predictions for short positions with (implemented in pytorch)
        yang_model : nn.Module
            model to make predictions for long positions with (implemented in pytorch)
        n_inputs : int
            number of inputs that the model expects
        yin_threshold : float (optional, default to 0.5)
            threshold value to make a positive prediction for the yin_model
        yang_threshold : float (optional, default to 0.5)
            threshold value to make a positive prediction for the yang_model
        yin_ceiling_threshold : int (optional, default to 1)
            the ceiling score for the yin model to have for the yang model to make a positive prediction
        yang_ceiling_threshold : int (optional, default to 1)
            the ceiling score for the yang model to have for the yin model to make a positive prediction
        '''
        self.yin_model = yin_model
        self.yang_model = yang_model
        self.n_inputs = n_inputs
        self.yin_threshold = yin_threshold
        self.yang_threshold = yang_threshold
        self.yin_ceiling_threshold = yin_ceiling_threshold
        self.yang_ceiling_threshold = yang_ceiling_threshold


    @classmethod
    def load_model(
        yin_yang_prophets,
        yin_model_class,
        yang_model_class, 
        yin_model_path,
        yang_model_path,
        n_inputs,
        yin_threshold=0.5,
        yang_threshold=0.5,
        yin_ceiling_threshold=1,
        yang_ceiling_threshold=1
    ):
        '''
        inits a yin_yang_prophets object along with a the models

        Parameters
        -----------
        yin_model_class : class
            the yin model class to be created
        yang_model_class : class
            the yang model class to be created
        yin_model_path : str
            the location of the weights of the yin model
        yang_model_path : str
            the location of the weights of the yang model
        n_inputs : int
            number of inputs that the model expects
        yin_threshold : float (optional, default to 0.5)
            threshold value to make a positive prediction for the yin_model
        yang_threshold : float (optional, default to 0.5)
            threshold value to make a positive prediction for the yang_model
        yin_ceiling_threshold : int (optional, default to 1)
            the ceiling score for the yin model to have for the yang model to make a positive prediction
        yang_ceiling_threshold : int (optional, default to 1)
            the ceiling score for the yang model to have for the yin model to make a positive prediction
        '''
        # init yin and yang models
        yin_model = yin_model_class()
        yin_model.load_model(yin_model_path)
        yang_model = yang_model_class()
        yang_model.load_model(yang_model_path)

        return yin_yang_prophets(
            yin_model,
            yang_model,
            n_inputs,
            yin_threshold=yin_threshold,
            yang_threshold=yang_threshold,
            yin_ceiling_threshold=yin_ceiling_threshold,
            yang_ceiling_threshold=yang_ceiling_threshold
        )

    def predict(self, sample):
        '''
        predict, given a sample

        Parameters
        -----------
        sample : np.array
            the sample to make prediction on 

        Returns
        -----------
        output : int
            prediction made, -1 for short position, 0 for nothing, 1 for long position
        score : float
            prediction score
        '''
        # get models and set to evaluation mode
        yin_model = self.yin_model
        yang_model = self.yang_model
        yin_model.eval()
        yang_model.eval()

        # put the sample through both models
        yin_output = yin_model(sample).item()
        yang_output = yang_model(sample).item()

        # init the scores
        score = yang_output
        output = 0

        # if the yin output is greater than the threshold, and if the yang output is lower than the ceiling threshold...
        if yin_output > self.yin_threshold and yang_output < self.yang_ceiling_threshold:
            # return output as -1 and set the score to the yin_output
            output = -1
            score = yin_output

        # if the yang output is greater than the threshold, and if the ying output is lower than the ceiling threshold...
        elif yin_output < self.yin_ceiling_threshold and yang_output > self.yang_threshold:
            # return output as 1 and set the score to the yang_output
            output = 1
            score = yang_output

        return output, score

    def predict_proba(self, sample):
        '''
        predict the probability
        '''
        # get the models and set to evaluation mode
        yin_model = self.yin_model
        yang_model = self.yang_model
        yin_model.eval()
        yang_model.eval()

        # put the sample through both models
        yin_output = yin_model(sample)
        yang_output = yang_model(sample)

        # this is the score implementation at the moment
        # possible implementation
        # - compare values and return higher
        # - compare difference between scores with corresponding thresholds
        score = yang_output * (1-yin_output)
        return score





















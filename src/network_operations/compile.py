from __init__ import *
from network_operations.retrieval import *

import pandas as pd
import numpy as np
'''
This script contains functions that compiles the csv data into dataframes before feature extraction and training.
Each row of the dataframe after compilation contains consecutive data points in terms of time granularity.
'''

def compile_consecutive_data(df, consecutive, granularity_key):
	'''
	Compiles the data retrieved from csvs into rows of consecutive datapoints.

	Parameters
	-----------
	df : pd.DataFrame
		dataframe that is retrieved from retrieval.py
	consecutive : int
		number of consecutive data points that each row should contain
	granularity_key : str
		The key of the granularity defined in the ctime script; to be used to addition when checking for consecutive datapoints.

	Returns
	-----------
	df : pd.DataFrame
		df of compiled datapoints
	'''
	if len(df) ==  0:
		raise Exception('No data retrieved, terminating.')

	data = []
	
	# loop through original df
	for i in range(len(df)-consecutive+1):
		# extract consecutive number of rows 
		segment = df.iloc[i: i+consecutive]

		# add to array if extracted segment is consecutive
		if __is_segment_consecutive(segment, consecutive, granularity_key):
			data.append(segment.values)

	# reshape datapoints 
	data = np.array(data).astype(float).reshape((len(data), -1))

	# compile new data into new dataframe with new headers
	df = pd.DataFrame(data, columns=__remake_headers(df, consecutive))
	return df

def __remake_headers(df, consecutive):
	'''
	Remakes the column headers of the original dataframe into new headers that are suitable for the new dataframe that contains consecutive datapoints per row.

	Parameters
	-----------
	df : pd.dataframe
		original df to extract headers from
	consecutive : int
		number of consective datapoints each row will have in new df

	Returns
	----------
	new_headers : list
		a list of new headers suitable for the new df 
	'''
	# extract column header of original df
	headers = list(df.columns)

	# loop through all original headers, and append {i} to the end of the header
	new_headers = []
	for i in range(consecutive):
		new_headers += ['{}{}'.format(header, i) for header in headers]

	return new_headers

def __is_segment_consecutive(segment, consecutive, granularity_key):
	'''
	Checks if the data in all rows of a segment (of a dataframe) is consecutive, with time and open-close price

	Parameters
	-----------
	segment : pd.Dataframe
		A segment of the retrieved dataframe to check
	consecutive : int
		number of consecutive rows to check for
	granularity_key : str
		The key of the granularity defined in the ctime script; to be used to addition when checking for consecutive datapoints.

	Returns
	-----------
	: bool
		returns true if all data is consecutive, false if otherwise
	'''
	# check length of segment
	if len(segment) != consecutive: raise Exception('Segment is of invalid length')

	# loop through each row to check that it differs from the next by the granularity value
	for i in range(consecutive-1):
		if ctime(segment.iloc[i]['time']).add_with_key(granularity_key) != ctime(segment.iloc[i+1]['time']):
			return False
		if abs(segment.iloc[i]['close'] - segment.iloc[i+1]['open']) > 0.001:
			return False
			
	return True

def __test():
	granularity_key = '15_minute'
	start = '20210801000000'
	instrument = INSTRUMENTS[0]
	consecutive = 8
	verbose = False

	df = retrieve_csv_for_month(granularity_key, start, instrument, verbose=verbose)
	df = compile_consecutive_data(df, consecutive, granularity_key)

	print(df)

if __name__ == '__main__':
	__test()
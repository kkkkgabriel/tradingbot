from dateutil.parser import isoparse
from datetime import datetime
import numpy as np

# global vars
N_DAYS_IN_MONTH = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
GRANULARITY_ADDITION = {
	'30_second': (30, 's'),
	'1_minute': (1, 'm'),
	'2_minute': (2, 'm'),
	'4_minute': (4, 'm'),
	'5_minute': (5, 'm'),
	'10_minute': (10, 'm'),
	'15_minute': (15, 'm'),
	'30_minute': (30, 'm'),
	'1_hour': (1, 'h'),
	'2_hour': (2, 'h'),
	'3_hour': (3, 'h'),
	'4_hour': (4, 'h'),
	'6_hour': (6, 'h'),
	'8_hour': (8, 'h'),
	'12_hour': (12, 'h'),
	'1_day': (1, 'D'),
	'1_week': (7, 'D'),
	'1_month': (1, 'M')
}

'''
ctime is short for custom time, which is allows for parsing for time and return time in formats that I want.
'''
class ctime:
	def __init__(self, time):
		'''
		Initialises a ctime object with a custom time string.

		Parameters
		-----------
		time : str/np.integer
			YYMMDDhhmmss. For eg, 2021101104352 creates a time object at 2021-11-10 10:43:52hrs
		'''
		if isinstance(time, int) or isinstance(time, np.integer):
			time = str(time)

		# check that string is a str
		if isinstance(time, str): 
			# check len of string
			if len(time) != 14: raise Exception('Invalid length of time string')
		
			# parse string and set self variables
			self.year = int(time[0:4])
			self.month = int(time[4:6])
			self.day = int(time[6:8])
			self.hour = int(time[8:10])
			self.minute = int(time[10:12])
			self.second = int(time[12:14])

		else:
			raise Exception('Argument is not a string/int')

	@classmethod
	def from_rfc3339(ctime, rfc3339):
		'''
		Initialises a ctime object from rfc3339

		Parameters
		-----------
		rfc3339 : str
			time string in rfc3339 format, for eg: 2021-09-17T09:35:00.000000000Z

		Returns
		-----------
		: ctime
			ctime object from rfc3339 string 
		'''
		s = '{:04d}{:02d}{:02d}{:02d}{:02d}{:02d}'.format(
			isoparse(rfc3339).year,
			isoparse(rfc3339).month,
			isoparse(rfc3339).day,
			isoparse(rfc3339).hour,
			isoparse(rfc3339).minute,
			isoparse(rfc3339).second
		)
		return ctime(s)

	@classmethod
	def from_now(ctime):
		'''
		Initialises a ctime object from current datetime

		Returns
		------------
		: ctime
			ctime object from current time 
		'''
		current_time = datetime.now().strftime("%Y%m%d%H%M%S")
		return ctime(current_time)
		

	def to_rfc3339_date(self):
		'''
		returns rfc3339 date string (YYYY-MM-DD) corresponding to ctime object , Eg: 2021-09-12
		'''
		return '{:04d}-{:02d}-{:02d}'.format(self.year, self.month, self.day)

	def to_rfc3339_datetime(self):
		return '{:04d}-{:02d}-{:02d}T{:02d}:{:02d}:{:02d}+08:00'.format(self.year, self.month, self.day, self.hour, self.minute, self.second)

	def to_custom_date(self):
		'''
		returns custom date string (YYYYMMDD) corresponding to ctime object, Eg: 20210912
		'''
		return '{:04d}{:02d}{:02d}'.format(self.year, self.month, self.day)

	def to_custom_time(self):
		return '{:02d}:{:02d}'.format(self.hour, self.minute)

	def to_custom_datetime(self):
		'''
		returns custom datetime string (YYYYMMDDhhmmss) corresponding to ctime object, Eg: 20210912081212
		'''
		return '{:04d}{:02d}{:02d}{:02d}{:02d}{:02d}'.format(self.year, self.month, self.day, self.hour, self.minute, self.second)

	def __str__(self):
		'''
		str magic method to return ctime object as custom datetime
		'''
		return '{:04d}{:02d}{:02d}{:02d}{:02d}{:02d}'.format(
			self.year,
			self.month,
			self.day,
			self.hour,
			self.minute,
			self.second,
		)

	def __eq__(self, other):
		'''
		magic method to compare ctime objects with the == operator. ctime objects are considered equal iff year, month, day, hour, minute and seconds are the same.
		'''
		return self.year == other.year and \
			self.month == other.month and \
			self.day == other.day and \
			self.hour == other.hour and \
			self.minute == other.minute and \
			self.second == other.second

	def __gt__(self, other):
		'''
		magic method to compare ctime objects with the > operator. A ctime object is considered greater than another, if the former's time is later than the latter's.
		'''
		if not isinstance(other, ctime): raise Exception("other object is not a (custom) time")

		if self == other:
			return False

		if self.year > other.year:
			return True
		elif self.year < other.year:
			return False

		if self.month > other.month:
			return True
		elif self.month < other.month:
			return False

		if self.day > other.day:
			return True
		elif self.day < other.day:
			return False

		if self.hour > other.hour:
			return True
		elif self.hour < other.hour:
			return False

		if self.minute > other.minute:
			return True
		elif self.minute < other.minute:
			return False

		if self.second > other.second:
			return True
		elif self.second < other.second:
			return False

		raise Exception("wtf?")

	def __check_leap_year(self, year):
		'''
		returns true if ctime is a leap year, false if otherwise
		'''
		return year % 4 == 0

	def __ge__(self, other):
		'''
		magic method for the >= operator. Returns true if a ctime is later than or equal the latter.
		'''
		return self == other or self > other

	def __lt__(self, other):
		'''
		magic method for the < operator. Returns true of a ctime is of a earlier time than the other.
		'''
		return not(self >= other)

	def __le__(self, other):
		'''
		magic method for the <= operator. Returns true if a ctime is earlier of equal to the latter.
		'''
		return self == other or self < other

	def add_with_key(self, key):
		'''
		Allows addition for ctime object with a granularity key
		'''
		return self.add(*GRANULARITY_ADDITION[key])

	def add(self, value, denomination):
		'''
		Allows addition for ctime object

		Parameters
		-----------
		value : int
			the value to add a denomination
		denomination : str
			the denomination to add to, possible values:
				'Y' -> year
				'M' -> month
				'D' -> day
				'h' -> hour
				'm' -> minute
				's' -> second

		Returns
		-----------
		the ctime object after addition
		''' 
		# for 'Y' denomination, add value to year
		if denomination == 'Y':
			self.year += value

		# for 'M' denomination, add value to month
		elif denomination == 'M':
			self.month += value

			# correct overflows from month to year
			while self.month > 12:
				self.year += 1
				self.month -= 12
				self.add(0, 'Y')

		# for 'D' denomination, add value to day
		elif denomination == "D":
			self.day += value

			# correct overflows from day to month (for leap year)
			if self.__check_leap_year(self.year) and self.day > 29 and self.month == 2:
				self.day -= 29
				self.month += 1

			# correct overflow from day to month (for non-leap year)
			while self.day > N_DAYS_IN_MONTH[self.month-1]:
				self.day -= N_DAYS_IN_MONTH[self.month-1]
				self.month += 1
				self.add(0, 'M')

		# For 'h' denomination, add value to hour
		elif denomination == "h":
			self.hour += value

			# correct overflows
			while self.hour > 23:
				self.hour -= 24
				self.day += 1
				self.add(0, 'D')

		# for the 'm' denomination
		elif denomination == "m":
			self.minute += value

			# correct overflow from minute to hour
			while self.minute > 59:
				self.minute -= 60
				self.hour += 1
				self.add(0, 'h')

		# for the 's' denomination, add value to the seconds
		elif denomination == "s":
			self.second += value

			# correct overflow from seconds to minutes
			while self.second > 59:
				self.second -= 60
				self.minute += 1
				self.add(0, 'm')
		else:
			raise Exception('wtf?')
		return self


def test():
	# from rfc3339 
	print('---- Test from rfc3339 string -----')
	t = ctime.from_rfc3339('2021-09-17T09:35:00.000000000Z') # 2021-09-17 09:35:00
	t2 = ctime.from_rfc3339('2021-09-15T16:15:00.000000000Z') # 2021-09-15 16:15:00
	t3 = ctime.from_rfc3339('2021-09-15T16:15:00.000000000Z') # 2021-09-15 16:15:00

	print(str(t))
	print(str(t2))
	print(str(t3))

	print(t == t2) # False
	print(t2 == t3) # True
	print(t > t3) # True
	print(t3 > t) # False
	print(t2 >= t3) # True
	print(t < t2) # False
	print(t3 <= t2) # True

	print()

	# from custom string
	print('---- Test from custom string -----')
	t = ctime('20210917093500') # 2021-09-17 09:35:00
	t2 = ctime('20210915161500') # 2021-09-15 16:15:00
	t3 = ctime('20210915161500') # 2021-09-15 16:15:00

	print(t.day)

	print(str(t))
	print(str(t2))
	print(str(t3))

	print(t == t2) # False
	print(t2 == t3) # True
	print(t > t3) # True
	print(t3 > t) # False
	print(t2 >= t3) # True
	print(t < t2) # False
	print(t3 <= t2) # True

	# from now
	t = ctime.from_now()
	print(str(t))

if __name__ == '__main__':
	test()


LENGTH_IN_DAY = {
	'30_second': 2880,
	'1_minute': 1440,
	'2_minute': 720,
	'4_minute': 360,
	'5_minute': 288,
	'10_minute': 144,
	'15_minute': 96,
	'30_minute': 48,
	'1_hour': 24,
	'2_hour': 12,
	'3_hour': 8,
	'4_hour': 6,
	'6_hour': 4,
	'8_hour': 3,
	'12_hour': 2,
	'1_day': 1,
	'1_week': 1,
	'1_month': 1
}

TRAIN_TEST_VAL_RATIO = (7,2,1)

def pip_to_currency(n):
    return n * 0.0001


from __init__ import *
from database.db_connection import DB_ENV

import os

class Logger:
	def __init__(self):
		self.log_file = None
		self.__init_log_file()

	@classmethod
	def log(Logger, writer, message):
		l=Logger()
		l.__write(writer, message)
		l.__close()

	def __init_log_file(self):
		if not DB_ENV:
			f = open(DEV_LOGS_FOLDER + ctime.from_now().to_rfc3339_date(), 'a')
		else:
			f = open(PROD_LOGS_FOLDER + ctime.from_now().to_rfc3339_date(), 'a')
		self.log_file = f

	def __write(self, writer, message):
		content = ''
		if not DB_ENV:
			content += '[DEV] '
		content += "{} ({}): {}\n".format(writer, str(ctime.from_now()), message)

		self.log_file.write(content)

	def __close(self):
		self.log_file.close()

